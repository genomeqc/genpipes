#!/usr/bin/env python

################################################################################
# Copyright (C) 2014, 2015 GenAP, McGill University and Genome Quebec Innovation Centre
#
# This file is part of MUGQIC Pipelines.
#
# MUGQIC Pipelines is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MUGQIC Pipelines is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MUGQIC Pipelines.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

# Python Standard Modules
import logging
import os
import sys

# Append mugqic_pipelines directory to Python library path
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))

# MUGQIC Modules
from core.config import *
from core.job import *
from bfx.readset import *

from bfx import blast
from bfx import mummer
from bfx import smrt_tools
from bfx import bedops
from pipelines import common

log = logging.getLogger(__name__)

class SequelAssembly(common.MUGQICPipeline):
    """
    PacBio Sequel Assembly Pipeline
    ========================

    """

    def __init__(self, protocol=None):
        self._protocol=protocol
        self.argparser.add_argument("-r", "--readsets", help="readset file", type=file)
        self.argparser.add_argument("-sc", "--seedcoverage", help="Seed coverage cutoff", type=int)
        super(SequelAssembly, self).__init__(protocol)

    @property
    def readsets(self):
        if not hasattr(self, "_readsets"):
            if self.args.readsets:
                self._readsets = parse_pacbio_readset_file(self.args.readsets.name)
            else:
                self.argparser.error("argument -r/--readsets is required!")

        return self._readsets

    @property
    def seedcoverage(self):
        if not hasattr(self, "_seedcoverage"):
            if self.args.seedcoverage:
                self._seedcoverage = self.args.seedcoverage
            else:
                self.argparser.error("argument -sc/--seedcoverage is required!")

        return self._seedcoverage

    def create_inputs(self):
        estimated_genome_size = self.samples[0].readsets[0].estimated_genome_size
        inputs_job = smrt_tools.create_inputs("inputs.json", "myInputs.json", estimated_genome_size, self.seedcoverage)
        inputs_job.name = "inputs"
        return [inputs_job]

    def create_dataset(self):
        files = []

        for sample in self.samples:
            for readset in sample.readsets:
                files.append(readset.bam_file)

        dataset_create_job = smrt_tools.dataset_create("ConsensusReadSet", "consensusreadset.xml", files)
        dataset_create_job.name = "create_dataset"
        return [dataset_create_job]

    def assembly(self):
        thread = config.param('assembly', 'thread', type='posint')

        assembly_job = smrt_tools.pb_microbial_analysis("consensusreadset.xml", thread)
        assembly_job.name = "pb_microbial_analysis"

        copy_job = Job(
            ["cromwell_out/outputs/basemods.gff", "cromwell_out/outputs/motifs.csv"],
            ["report/basemod.gff", "report/motifs.csv"],
            command="cp cromwell_out/outputs/basemods.gff report/basemod.gff && cp cromwell_out/outputs/motifs.csv report/",
            name="basemodification.copyFiles"
        )

        return [assembly_job, copy_job]

    def assembly_stats(self):

        coverage_json_job = smrt_tools.json_metrics_to_csv("cromwell_out/outputs/coverage.report.json", "report/coverage.csv")
        coverage_json_job.name="report.coverage"

        mapping_json_job = smrt_tools.json_metrics_to_csv("cromwell_out/outputs/mapping_stats.report.json", "report/mapping.csv")
        mapping_json_job.name="report.mapping"

        polished_assembly_json_job = smrt_tools.json_metrics_to_csv("cromwell_out/outputs/polished_assembly.report.json", "report/polished_assembly.csv")
        polished_assembly_json_job.name="report.polished_assembly"

        copy_job = Job(
            ["cromwell_out/outputs/coverage.report.json",
             "cromwell_out/outputs/mapping_stats.report.json", "cromwell_out/outputs/polished_assembly.report.json",
             "cromwell_out/outputs/assembly.rotated.polished.renamed.fsa"],
            ["report/coverage.report.json",
             "report/mapping_stats.report.json", "report/polished_assembly.report.json", "report/consensus.fasta"],
            command="cp cromwell_out/outputs/*.report.json report/ && cp cromwell_out/outputs/assembly.rotated.polished.renamed.fsa report/consensus.fasta",
            name="report.copyFiles"
        )

        report_directory="report"
        report_file = os.path.join(report_directory, "SequelAssembly.stats.md")
        report_job = Job(
            ["report/coverage.csv", "report/mapping.csv", "report/polished_assembly.csv"],
            [report_file],
            [['DEFAULT', 'module_pandoc']],
            command="""\
coverage_table=`awk -F"," '{{OFS=","; if (NR==1) {{print; gsub(/[^,]/, "-")}} print}}' report/coverage.csv | sed 's/|/\\\\\\\\|/g' | sed 's/,/|/g' | sed 's/"//g'`
mapping_table=`awk -F"," '{{OFS=","; if (NR==1) {{print; gsub(/[^,]/, "-")}} print}}' report/mapping.csv | sed 's/|/\\\\\\\\|/g' | sed 's/,/|/g' | sed 's/"//g'`
polished_assembly_table=`awk -F"," '{{OFS=","; if (NR==1) {{print; gsub(/[^,]/, "-")}} print}}' report/polished_assembly.csv | sed 's/|/\\\\\\\\|/g' | sed 's/,/|/g' | sed 's/"//g'`
pandoc --to=markdown \\
--template {report_template_dir}/{basename_report_file} \\
--variable coverage_table="$coverage_table" \\
--variable mapping_table="$mapping_table" \\
--variable polished_assembly_table="$polished_assembly_table" \\
{report_template_dir}/{basename_report_file} \\
> {report_file}""".format(
                report_directory=report_directory,
                report_template_dir=self.report_template_dir,
                basename_report_file=os.path.basename(report_file),
                report_file=report_file
            ),
            report_files=[report_file],
            name="report.makereport")

        return [coverage_json_job, mapping_json_job, polished_assembly_json_job, copy_job, report_job]

    def blast(self):
        """
        Blast polished assembly against nr using dc-megablast.
        """

        jobs = []

        blast_directory = "blast"
        blast_report = os.path.join(blast_directory, "blast_report")

        jobs.append(bedops.gff2bed("cromwell_out/outputs/coverage.gff", "cromwell_out/outputs/coverage.bed"))

        # Blast contigs against nt
        jobs.append(concat_jobs([
            Job(command="mkdir -p " + blast_directory, samples=self.samples),
            blast.dcmegablast(
                "cromwell_out/outputs/final_assembly.fasta",
                "7",
                blast_report,
                "cromwell_out/outputs/coverage.bed",
                blast_directory
            )
        ], name="blast_dcmegablast"))

        # Get fasta file of best hit.
        job = blast.blastdbcmd(
            blast_report,
            "$(grep -v '^#' < " + blast_report + " | head -n 1 | awk -F '\\t' '{print $2}' | sed 's/gi|\([0-9]*\)|.*/\\1/' | tr '\\n' '  ')",
            os.path.join(blast_directory, "nt_reference.fasta"),
            )
        job.name = "blast_blastdbcmd"
        job.samples=self.samples
        jobs.append(job)

        report_directory = "report"
        report_file = os.path.join(report_directory, "SequelAssembly.blast.md")
        jobs.append(
            Job(
                [os.path.join(blast_directory, "blastCov.tsv"), os.path.join(blast_directory, "contigsCoverage.tsv")],
                [report_file],
                [['blast', 'module_pandoc']],
                command="""\
cp {blast_directory}/blastCov.tsv {report_directory}/ && \\
cp {blast_directory}/contigsCoverage.tsv {report_directory}/ && \\
blast_table=`awk -F"\t" '{{OFS="\t"; if (NR==1) {{print; gsub(/[^\t]/, "-")}} print}}' {report_directory}/blastCov.tsv | sed 's/|/\\\\\\\\|/g' | sed 's/\t/|/g' | head -21` && \\
pandoc --to=markdown \\
--template {report_template_dir}/{basename_report_file} \\
--variable blast_table="$blast_table" \\
{report_template_dir}/{basename_report_file} \\
> {report_file}""".format(
                    blast_directory=blast_directory,
                    report_directory=report_directory,
                    report_template_dir=self.report_template_dir,
                    basename_report_file=os.path.basename(report_file),
                    report_file=report_file
                ),
                report_files=[report_file],
                name="blast_report")
        )
        log.warn(self.report_template_dir)

        return jobs

    def mummer(self):
        """
        Using MUMmer, align polished assembly against best hit from blast job. Also align polished assembly against itself to detect structure variation such as repeats, etc.
        """

        jobs = []

        fasta_reference = os.path.join("blast", "nt_reference.fasta")
        mummer_directory = "mummer"
        mummer_file_prefix = mummer_directory + os.path.sep

        fasta_consensus = "cromwell_out/outputs/final_assembly.fasta"

        # Run nucmer
        jobs.append(concat_jobs([
            Job(command="mkdir -p " + mummer_directory, samples=self.samples),
            mummer.reference(
                mummer_file_prefix + "nucmer",
                fasta_reference,
                fasta_consensus,
                "nucmer",
                mummer_file_prefix + "nucmer.delta",
                mummer_file_prefix + "nucmer.delta",
                mummer_file_prefix + "dnadiff",
                mummer_file_prefix + "dnadiff.delta",
                mummer_file_prefix + "dnadiff.delta.snpflank"
            )
        ], name="mummer_reference"))

        jobs.append(concat_jobs([
            Job(command="mkdir -p " + mummer_directory, samples=self.samples),
            mummer.self(
                mummer_file_prefix + "nucmer.self",
                fasta_consensus,
                "-self",
                mummer_file_prefix + "nucmer.self.delta",
                mummer_file_prefix + "nucmer.self.delta"
            )
        ], name="mummer_self"))

        report_directory = "report"
        report_file = os.path.join(report_directory, "SequelAssembly.mummer.md")
        jobs.append(
            Job(
                [mummer_file_prefix + "nucmer.self.delta.png", mummer_file_prefix + "nucmer.delta.png"],
                [report_file],
                [['mummer', 'module_pandoc']],
                command="""\
cp {mummer_file_prefix}nucmer.self.delta.png {mummer_file_prefix}nucmer.delta.png {report_directory}/ && \\
pandoc --to=markdown \\
--template {report_template_dir}/{basename_report_file} \\
{report_template_dir}/{basename_report_file} \\
> {report_file}""".format(
                    mummer_file_prefix=mummer_file_prefix,
                    report_directory=report_directory,
                    report_template_dir=self.report_template_dir,
                    basename_report_file=os.path.basename(report_file),
                    report_file=report_file
                ),
                report_files=[report_file],
                name="mummer_report")
        )

        return jobs

    @property
    def steps(self):
        return [
            self.create_inputs,
            self.create_dataset,
            self.assembly,
            self.assembly_stats,
            self.blast,
            self.mummer
        ]

if __name__ == '__main__':
    SequelAssembly()
