#!/usr/bin/env python

################################################################################
# Copyright (C) 2014, 2015 GenAP, McGill University and Genome Quebec Innovation Centre
#
# This file is part of MUGQIC Pipelines.
#
# MUGQIC Pipelines is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MUGQIC Pipelines is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MUGQIC Pipelines.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

# Python Standard Modules
from __future__ import print_function, division, unicode_literals, absolute_import
import os
import glob
import sys
import itertools
import math

# Append mugqic_pipelines directory to Python library path
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))

# MUGQIC Modules
from bfx.readset import *
from bfx import smrt_tools, unix_tools
from bfx import nanuq_tools
from pipelines import common

log = logging.getLogger(__name__)


class SequelRunProcessing(common.MUGQICPipeline):
    """
        Sequel Run Processing Pipeline
        ================================


    """

    def __init__(self, protocol=None):
        self._protocol=protocol
        self.copy_job_inputs = []
        self.argparser.add_argument("-r", "--readsets",
                                    help="nanuq readset file. The default file is 'nanuq.fa' in the output folder. Will be automatically downloaded if not present.",
                                    type=file, required=False)
        self.argparser.add_argument("-i",
                                    help="nanuq dictionary file. The default file is 'nanuq.dict' in the output folder. Will be automatically downloaded if not present",
                                    type=file, required=False,
                                    dest="dictionary_file")
        self.argparser.add_argument("-w", "--force-download",
                                    help="force the download of the samples sheets (default: false)",
                                    action="store_true",
                                    dest="force_download")

        super(SequelRunProcessing, self).__init__(protocol)

    @property
    def readsets(self):
        if not hasattr(self, "_readsets"):
            self._readsets = self.load_readsets()
        return self._readsets

    def main_readset(self):
        return [rs for rs in self.readsets if rs.is_main][0]

    def unlabeled_readset(self):
        return [rs for rs in self.readsets if rs.is_unlabeled][0]

    def cell_number(self):
        return os.path.basename(self.output_dir)

    @property
    def file_prefix(self):
        if not hasattr(self, "_file_prefix"):
            self._file_prefix = self.extract_file_prefix()
        return self._file_prefix

    @property
    def barcode_fasta_file(self):
        return self.args.readsets if self.args.readsets else self.output_dir + os.sep + "nanuq.fa"

    @property
    def dictionary_file(self):
        return self.args.dictionary_file if self.args.dictionary_file else self.output_dir + os.sep + "nanuq.dict"

    def extract_file_prefix(self):
        file_name = self.output_dir + os.path.sep + "m*.scraps.bam"
        files = glob.glob(file_name)
        return files[0].split(".")[0].split(os.path.sep)[-1]

    @property
    def run_id(self):
        """ The run id from the run folder.

        """
        if not hasattr(self, "_run_id"):
            self._run_id = os.path.basename(os.path.dirname(self.output_dir))

        return self._run_id

    @property
    def nanuq_readset_file(self):
        return self.args.readsets.name if self.args.readsets else self.output_dir + os.sep + "run.nanuq.csv"

    @property
    def steps(self):
        return [
            self.ccs,
            self.lima,
            self.whitelists,
            self.split,
            self.blast,
            self.qc,
            self.md5,
            self.copy,
            self.end_copy_notification
        ]

    def ccs(self):
        """

        """
        jobs = []
        main_rs = self.main_readset()

        nb_threads = config.param('ccs', 'threads')
        nb_passes = config.param('ccs', 'passes')

        ccs_jobs = []
        # Notifications
        start_notification_command = config.param('ccs', 'start_notification_command', required=False)
        if start_notification_command:
            output_start_notification = self.output_dir + os.sep + "notificationStart." + self.cell_number() + ".out"
            notification_job = Job(command=start_notification_command.format(
                technology=config.param('copy', 'technology'),
                output_dir=self.output_dir,
                run_id=self.run_id,
                output=output_start_notification,
                lane_number=self.cell_number()
            ))
            ccs_jobs.append(notification_job)

        ccs_job = smrt_tools.ccs(main_rs.subreads_file, main_rs.ccs_file, nb_threads, nb_passes)
        ccs_job.samples = [main_rs]
        ccs_jobs.append(ccs_job)
        jobs.append(concat_jobs(ccs_jobs, name="ccs." + self.run_id + "_" + self.cell_number()))
        self.add_copy_job_inputs(jobs)

        end_notification_command = config.param('ccs', 'end_notification_command', required=False)
        if end_notification_command:
            output1 = self.output_dir + os.sep + "notificationCCSEnd." + self.cell_number() + ".out"
            job = Job([main_rs.ccs_file], [output1],
                      name="end_ccs_notification." + self.run_id + "." + self.cell_number())
            job.command = end_notification_command.format(
                technology=config.param('copy', 'technology'),
                output_dir=self.output_dir,
                run_id=self.run_id,
                output=output1,
                lane_number=self.cell_number()
            )
            jobs.append(job)

        return jobs

    def lima(self):
        """

        """
        jobs = []

        # only one lima job is needed for all readsets
        if len(self.readsets) > 1:
            main_rs = self.main_readset()

            file_prefix = self.file_prefix
            is_symmetric = len([rs for rs in self.readsets if rs.is_symmetric]) > 0
            nb_threads = config.param('lima', 'threads')
            job = smrt_tools.lima(main_rs.ccs_file,
                                  os.path.join(self.output_dir, file_prefix + ".bc.ccs.bam"),
                                  nb_threads,
                                  self.barcode_fasta_file,
                                  is_symmetric)
            job.name = "lima." + self.run_id + "_" + self.cell_number()
            job.samples = [main_rs]
            jobs.append(job)

        self.add_copy_job_inputs(jobs)
        return jobs

    def whitelists(self):
        """

        """
        jobs = []
        nanuq_jar = config.param('whitelists', 'jar')
        outputs = [rs.whitelist_file for rs in self.readsets if not rs.is_main and not rs.is_unlabeled]
        if len(outputs) > 0:
            job = nanuq_tools.generate_whitelist_for_dictionary(
                nanuq_jar, self.dictionary_file, os.path.join(self.output_dir, self.file_prefix + ".bc.ccs.bam"), self.output_dir, outputs
            )
            job.name = "whitelists." + self.run_id + "_" + self.cell_number()
            job.samples = [self.main_readset()]
            jobs.append(job)
        self.add_copy_job_inputs(jobs)
        return jobs

    def split(self):
        """

        """
        jobs = []

        if len(self.readsets) > 1:
            nanuq_jar = config.param('split', 'jar')

            whitelists = [rs.whitelist_file for rs in self.readsets if not rs.is_main and not rs.is_unlabeled]
            unlabeled_rs = self.unlabeled_readset()
            input_ccs = os.path.join(self.output_dir, self.file_prefix + ".bc.ccs.bam")


            job = nanuq_tools.generate_bam_for_whitelist(nanuq_jar, input_ccs, self.output_dir, "ccs", unlabeled_rs.ccs_file, whitelists)
            job.name = "split.ccs." + self.run_id + "_" + self.cell_number()
            job.samples = [self.main_readset()]
            jobs.append(job)

            split_notification_command = config.param('split', 'split_notification_command', required=False)
            if split_notification_command:
                split_notification_output = self.output_dir + os.sep + "splitDemultiplexingEnd." + self.cell_number() + ".out"
                job = Job([unlabeled_rs.ccs_file], [split_notification_output],
                      name="split_demultiplexing_end." + self.run_id + "." + self.cell_number())
                job.command = split_notification_command.format(
                    technology=config.param('copy', 'technology'),
                    output_dir=self.output_dir,
                    run_id=self.run_id,
                    output=split_notification_output,
                    lane_number=self.cell_number()
                )
                jobs.append(job)

        self.add_copy_job_inputs(jobs)
        return jobs

    def blast(self):
        """
            Run blast on a subsample of the reads of each sample to find the 20 most
            frequent hits.

        """
        jobs = []

        nb_blast_to_do = config.param('blast', 'nb_blast_to_do', type="posint")
        nb_blast_to_do = max(1, (int(nb_blast_to_do) // len(self.readsets)))

        for readset in self.readsets:
            output_prefix = os.path.join(self.output_dir,
                                         "Blast_sample",
                                         readset.name)
            output = output_prefix + '.R1.RDP.blastHit_20MF_species.txt'
            current_jobs = [Job(command="mkdir -p " + os.path.dirname(output))]

            fasta_file = output_prefix + ".R1.subSampled_{nb_blast_to_do}.fasta".format(nb_blast_to_do=nb_blast_to_do)
            result_file = output_prefix + ".R1.subSampled_{nb_blast_to_do}.blastres".format(nb_blast_to_do=nb_blast_to_do)

            input = readset.ccs_file

            # count the read that aren't marked as secondary alignment and calculate the ratio of reads to subsample
            command = """subsampling=$(samtools view -F 0x0180 {input} | wc -l | awk -v nbReads={nb_blast_to_do} '{{if ($1 == 0) print "0.0001"; else x=sprintf("%.4f", nbReads/$1); if (x == "0.0000") print "0.0001"; else print x}}')""".format(
                input=input,
                nb_blast_to_do=nb_blast_to_do
            )
            current_jobs.append(Job([input], [], [["blast", "module_samtools"]], command=command, samples=[readset]))
            # subsample the reads and output to a temp fasta
            command = """samtools view -s $subsampling -F 0x0180 {input} | awk '{{OFS="\\t"; print ">"$1"\\n"$10}}' - > {fasta_file}""".format(
                input=input,
                fasta_file=fasta_file
            )
            current_jobs.append(Job([input], [], [["blast", "module_samtools"]], command=command))

            # run blast
            command = """blastn -query {fasta_file} -db nt -out {result_file} -perc_identity 80 -num_descriptions 1 -num_alignments 1""".format(
                fasta_file=fasta_file,
                result_file=result_file
            )
            current_jobs.append(Job([], [], [["blast", "module_blast"]], command=command))

            # filter and format the result to only have the sorted number of match and the species
            command = """grep ">" {result_file} | awk ' {{ print $2 "_" $3}} ' | sort | uniq -c | sort -n -r | head -20 > {output} && true""".format(
                result_file=result_file,
                output=output
            )
            current_jobs.append(Job([], [output], [], command=command))

            # merge all blast steps of the readset into one job
            job = concat_jobs(current_jobs,
                              name="blast." + readset.name + ".blast." + self.run_id + "." + self.cell_number())
            jobs.append(job)
        self.add_copy_job_inputs(jobs)
        return self.throttle_jobs(jobs)

    def qc(self):
        """

        """
        jobs = []
        barcoded_bam_jobs = []
        nanuq_jar = config.param('qc', 'jar')

        for rs in self.readsets:
            job = nanuq_tools.generate_read_length_histograms_for_bams(nanuq_jar, rs.ccs_file, self.output_dir)
            job.name = "qc." + self.run_id + "_" + self.cell_number()
            job.samples = [rs]
            if rs.is_main:
                jobs.append(job)
            else:
                barcoded_bam_jobs.append(job)

        if len(barcoded_bam_jobs) > 0:
            jobs.append(concat_jobs(barcoded_bam_jobs,  name="qc." + "barcoded." + self.run_id + "." + self.cell_number()))

        self.add_copy_job_inputs(jobs)
        return jobs

    def md5(self):
        """
            Create md5 checksum files using the system 'md5sum' util.

            One checksum file is created for each file.
        """
        jobs = []
        for readset in self.readsets:
            job = unix_tools.md5(readset.ccs_file, readset.ccs_file + ".md5")
            job.samples = [readset]
            jobs.append(job)

        job = concat_jobs(jobs, "md5." + self.run_id + "_" + self.cell_number())
        self.add_copy_job_inputs([job])
        return [job]

    def copy(self):
        """
            Copy processed files to another place where they can be served or loaded into a
            LIMS.

            The destination folder and the command used can be set in the configuration
            file.

            An optional notification can be sent before the copy. The command used is in the configuration file.
        """

        inputs = self.copy_job_inputs
        jobs_to_concat = []
        jobs = []

        # Notification
        notification_command = config.param('copy', 'notification_command', required=False)
        if notification_command:
            output1 = self.output_dir + os.sep + "notificationProcessingComplete." + self.cell_number() + ".out"
            job = Job(inputs, [output1],
                      name="start_copy_notification." + self.run_id + "." + self.cell_number())
            job.command = notification_command.format(
                technology=config.param('copy', 'technology'),
                output_dir=self.output_dir,
                run_id=self.run_id,
                output=output1,
                lane_number=self.cell_number()
            )
            jobs_to_concat.append(job)

        # Actual copy
        full_destination_folder = config.param('copy', 'destination_folder', type="dirpath") + self.run_id + os.sep + os.path.basename(self.output_dir)
        output = full_destination_folder + os.sep + "copyCompleted." + self.cell_number() + ".out"

        copy_command_output_folder = config.param('copy', 'copy_command', required=False).format(
            exclusion_clauses="",
            lane_number=self.cell_number(),
            run_id=self.run_id,
            source=self.output_dir,
        )
        job = Job(inputs, [output], command=copy_command_output_folder)
        job.samples = [self.main_readset()]
        jobs_to_concat.append(job)
        jobs_to_concat.append(Job(command="touch " + output))

        job = concat_jobs(jobs_to_concat, "copy." + self.run_id + "." + self.cell_number())
        jobs.append(job)

        output_metrics = self.output_dir + os.sep + "copyMetricsCompleted." + self.cell_number() + ".out"
        copy_metrics_command = config.param('copy', 'copy_metrics_command', required=False).format(
            source=self.output_dir,
            run_id=self.run_id,
            lane_number=self.cell_number(),
        )
        metrics_job = Job(inputs, [output_metrics], command=copy_metrics_command)
        metrics_job.samples = [self.main_readset()]
        jobs.append(concat_jobs([metrics_job, Job(command="touch " + output_metrics)],
                                "copy_metrics." + self.run_id + "." + self.cell_number()))

        return jobs

    def end_copy_notification(self):
        """
            Send an optional notification to notify that the copy is finished.

            The command used is in the configuration file. This step is skipped when no
            command is provided.
        """
        jobs = []


        full_destination_folder = config.param('copy', 'destination_folder', type="dirpath") + self.run_id + os.sep + os.path.basename(self.output_dir)

        input = [full_destination_folder + os.sep + "copyCompleted." + self.cell_number() + ".out",
                 self.output_dir + os.sep + "copyMetricsCompleted." + self.cell_number() + ".out"]
        output = full_destination_folder + os.sep + "notificationAssociation." + self.cell_number() + ".out"

        technology = config.param('end_copy_notification', 'technology')

        notification_command = config.param('end_copy_notification', 'notification_command', required=False)
        if notification_command:
            job = Job(input, [output], name="end_copy_notification." + self.run_id + "." + self.cell_number())
            job.command = notification_command.format(
                technology=technology,
                output_dir=self.output_dir,
                run_name=os.path.basename(self.output_dir),
                run_id=self.run_id,
                output=output,
                lane_number=self.cell_number()
            )
            jobs.append(job)

        return jobs

    #
    # Utility methods
    #

    def add_copy_job_inputs(self, jobs):
        for job in jobs:
            # we first remove dependencies of the current job, since we will have a dependency on that job
            self.copy_job_inputs = [item for item in self.copy_job_inputs if item not in job.input_files]
            self.copy_job_inputs.extend(job.output_files)



    def load_readsets(self):
        """
            Download the sample sheets if required or asked for; call the load of these files and return a list of
            readsets.
        """

        # Nanuq dictionary file
        if not self.args.dictionary_file or self.args.force_download:
            if not os.path.exists(self.dictionary_file) or self.args.force_download:
                command = config.param('DEFAULT', 'fetch_dict_command').format(
                    output_directory=self.output_dir,
                    run_id=self.run_id,
                    cell=self.cell_number(),
                    file_prefix=self.file_prefix,
                    filename=self.dictionary_file
                )
                log.info(command)
                return_code = subprocess.call(command, shell=True)
                if return_code != 0:
                    raise Exception("Unable to download the nanuq dictionary file.")

        # Nanuq fasta file
        if not self.args.readsets or self.args.force_download:
            if not os.path.exists(self.barcode_fasta_file) or self.args.force_download:
                command = config.param('DEFAULT', 'fetch_fasta_command').format(
                    output_directory=self.output_dir,
                    run_id=self.run_id,
                    cell=self.cell_number(),
                    filename=self.barcode_fasta_file
                )
                return_code = subprocess.call(command, shell=True)
                if return_code != 0:
                    raise Exception("Unable to download the Nanuq fasta file.")

        return parse_sequel_raw_readset_files(
            self.dictionary_file,
            self.output_dir,
            self.file_prefix
        )

    def submit_jobs(self):
        super(SequelRunProcessing, self).submit_jobs()

    def throttle_jobs(self, jobs):
        """ Group jobs of the same task (same name prefix) if they exceed the configured threshold number. """
        max_jobs_per_step = config.param('default', 'max_jobs_per_step', required=False, type="int")
        jobs_by_name = collections.OrderedDict()
        reply = []

        # group jobs by task (name)
        for job in jobs:
            jobs_by_name.setdefault(job.name.split(".", 1)[0], []).append(job)

        # loop on all task
        for job_name in jobs_by_name:
            current_jobs = jobs_by_name[job_name]
            if max_jobs_per_step and 0 < max_jobs_per_step < len(current_jobs):
                # we exceed the threshold, we group using 'number_task_by_job' jobs per group
                number_task_by_job = int(math.ceil(len(current_jobs) / max_jobs_per_step))
                merged_jobs = []
                for x in range(max_jobs_per_step):
                    if x * number_task_by_job < len(current_jobs):
                        merged_jobs.append(concat_jobs(
                            current_jobs[x * number_task_by_job:min((x + 1) * number_task_by_job, len(current_jobs))],
                            job_name + "." + str(x + 1) + "." + self.run_id + "." + self.cell_number()))
                reply.extend(merged_jobs)
            else:
                reply.extend(current_jobs)
        return reply


if __name__ == '__main__':
    pipeline = SequelRunProcessing()
