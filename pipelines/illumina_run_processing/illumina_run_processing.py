#!/usr/bin/env python

################################################################################
# Copyright (C) 2014, 2015 GenAP, McGill University and Genome Quebec Innovation Centre
#
# This file is part of MUGQIC Pipelines.
#
# MUGQIC Pipelines is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MUGQIC Pipelines is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MUGQIC Pipelines.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

# Python Standard Modules
from __future__ import print_function, division, unicode_literals, absolute_import
import os
import sys
import itertools
import xml.etree.ElementTree as Xml
import math
import re

# Append mugqic_pipelines directory to Python library path
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))

# MUGQIC Modules
from bfx.readset import *

from bfx import fastqc
from bfx import nanuq_tools
from bfx import picard
from pipelines import common

log = logging.getLogger(__name__)


class RunInfoRead(object):
    """ Model of a read from the Illumina sequencer.
        Those attributes can be found in the RunInfo.xml file.
    """

    def __init__(self, number, nb_cycles, is_index):
        self._number = number
        self._nb_cycles = nb_cycles
        self._is_index = is_index

    @property
    def number(self):
        return self._number

    @property
    def nb_cycles(self):
        return self._nb_cycles

    @property
    def is_index(self):
        return self._is_index

class IlluminaRunProcessing(common.MUGQICPipeline):
    """
        Illumina Run Processing Pipeline
        ================================

        The standard MUGQIC Illumina Run Processing pipeline uses the Illumina bcl2fastq
        software to convert and demultiplex the base call files to fastq files. The
        pipeline runs some QCs on the raw data, on the fastq and on the alignment.

        Sample Sheets
        -------------

        The pipeline uses two input sample sheets. The first one is the standard Casava
        sheet, a csv file having the following columns (please refer to the Illumina
        Casava user guide):

        - `SampleID`
        - `FCID`
        - `SampleRef`
        - `Index`
        - `Description`
        - `Control`
        - `Recipe`
        - `Operator`
        - `SampleProject`

        Example:

            FCID,Lane,SampleID,SampleRef,Index,Description,Control,Recipe,Operator,SampleProject
            H84WNADXX,1,sample1_MPS0001,,TAAGGCGA-AGAGTAGA,,N,,,nanuq
            H84WNADXX,1,sample47_MPS0047,,GTAGAGGA-CTAAGCCT,,N,,,nanuq


        The second sample sheet is called the Nanuq run sheet. It's a csv file with the
        following minimal set of mandatory columns (the column order in the file doesn't
        matter)

        - `ProcessingSheetId` Must be the same as the `SampleID` from the Casava Sheet.
        - `Name` The sample name put in RG headers of bam files and on filename on disk.
        - `Run` The run number.
        - `Region` The lane number.
        - `Library Barcode` The library barcode put in .bam's RG headers and on disk
        - `Library Source` The type of library. If this value contains `RNA` or `cDNA`,
        `STAR` will be used to make the aligmnent, otherwise, `bwa_mem` will be used
        - `Library Type` Used to determine is the sample is from cDNA/RNA when the
        `Library Source` is `Library`
        - `BED Files` The name of the BED file containing the genomic targets. This is
        the `filename` parameter passed to the `fetch_bed_file_command`
        - `Genomic Database` The reference used to make the alignment and calculate aligments metrics

        Example:

            Name,Genomic Database,Library Barcode,Library Source,Library Type,Run,Region,BED Files,ProcessingSheetId
            sample1,Rattus_norvegicus:Rnor_5.0,MPS0001,RNA,Nextera XT,1419,1,toto.bed,sample1_MPS0001
            sample47,,MPS1047,Library,Nextera XT,1419,2,toto.bed,sample47_MPS1047
    """

    def __init__(self, protocol=None):
        self._protocol=protocol
        self.copy_job_blast_inputs = []
        self.copy_job_md5_inputs = []
        self.copy_job_metrics_inputs = []
        self.copy_job_blast_optional_inputs = []
        self.copy_job_metrics_optional_inputs = []

        self.argparser.add_argument("-d", "--run", help="run directory", required=False, dest="run_dir")
        self.argparser.add_argument("--lanes", help="lane number", type=int, nargs="+", dest="lane_numbers")
        self.argparser.add_argument("-r", "--readsets",
                                    help="nanuq readset file. The default file is 'run.nanuq.csv' in the output folder. Will be automatically downloaded if not present.",
                                    type=file, required=False)
        self.argparser.add_argument("-i",
                                    help="illumina dragen sample sheet. The default file is 'DragenSampleSheet.nanuq.csv' in the output folder. Will be automatically downloaded if not present",
                                    type=file, required=False,
                                    dest="dragen_sheet_file")
        self.argparser.add_argument("-a",
                                    help="illumina casava sample sheet. The default file is 'SampleSheet.nanuq.csv' in the output folder. Will be automatically downloaded if not present",
                                    type=file, required=False,
                                    dest="casava_sheet_file")
        self.argparser.add_argument("-x",
                                    help="first index base to use for demultiplexing (inclusive). The index from the sample sheet will be adjusted according to that value.",
                                    type=int, required=False,
                                    dest="first_index")
        self.argparser.add_argument("-y", help="last index base to use for demultiplexing (inclusive)", type=int, required=False,
                                    dest="last_index")
        self.argparser.add_argument("-m", help="number of index mistmaches allowed for demultiplexing (default 1). Barcode collisions are always checked.",
                                    type=int,
                                    required=False, dest="number_of_mismatches")
        self.argparser.add_argument("-w", "--force-download",
                                    help="force the download of the samples sheets (default: false)",
                                    action="store_true",
                                    dest="force_download")
        self.argparser.add_argument("-g", "--merge-lanes",
                                    help="Merge the lanes (default: false)",
                                    action="store_true",
                                    dest="merge_lanes")
        self.argparser.add_argument("-u",
                                    help="extract the second index fastq, sometime used as molecular barcode",
                                    action="store_true",
                                    dest="extract_second_index")
        self.argparser.add_argument("--readset-onboard-filtering",
                                    help="Skip filtering of readsets based on the presence of onboard fastq and secondary_analysis",
                                    action="store_true",
                                    dest="readset_onboard_filtering"
        )

        super(IlluminaRunProcessing, self).__init__(protocol)

    @property
    def readsets(self):
        if not hasattr(self, "_readsets"):
            self._readsets = self.load_readsets()
            self.generate_illumina_lane_sample_sheet()
        return self._readsets

    @property
    def mask(self):
        if not hasattr(self, "_mask"):
            self._mask = self.get_mask()
        return self._mask

    @property
    def number_of_mismatches(self):
        return self.args.number_of_mismatches if (self.args.number_of_mismatches is not None) else 1

    @property
    def lane_numbers(self):
        if (self.args.lane_numbers):
            return self.args.lane_numbers
        else:
            raise Exception("Error: missing '--lanes' option!")

    def lanes_str(self):
        return "merged" if self.are_lanes_merged() else str(self.lane_numbers[0])

    def get_lanes_str(self):
        return "." + self.lanes_str()

    def are_lanes_merged(self):
        return self.args.merge_lanes or len(self.lane_numbers) > 1

    @property
    def is_paired_end(self):
        if not hasattr(self, "_is_paired_end"):
            self._is_paired_end = len([read_info for read_info in self.read_infos if (not read_info.is_index)]) > 1
        return self._is_paired_end

    @property
    def run_id(self):
        """ The run id from the run folder.
            Supports both default folder name configuration and GQ's globaly unique name convention.
        """
        if not hasattr(self, "_run_id"):
            if re.search(".*_\d+\wS\d\d[AB]", self.run_dir):
                m = re.search(".*/(\d+_[^_]+_\d+_[^_]+_(\d+)\wS.+)", self.run_dir)
                self._run_id = m.group(2)
            elif re.search(".*\d+_[^_]+_\d+_.+", self.run_dir):
                m = re.search(".*/(\d+_([^_]+_\d+)_.*)", self.run_dir)
                self._run_id = m.group(2)
            else:
                log.warn("Unsupported folder name: " + self.run_dir)

        return self._run_id

    @property
    def run_dir(self):
        if self.args.run_dir:
            return os.path.abspath(self.args.run_dir)
        else:
            raise Exception("Error: missing '-d/--run' option!")

    @property
    def casava_sheet_file(self):
        return self.args.casava_sheet_file.name \
            if self.args.casava_sheet_file else self.output_dir + os.sep + "SampleSheet.nanuq.csv"

    @property
    def dragen_sheet_file(self):
        return self.args.dragen_sheet_file.name \
            if self.args.dragen_sheet_file else self.output_dir + os.sep + "SampleSheetDragen.nanuq." + self.lanes_str() + ".csv"

    @property
    def nanuq_readset_file(self):
        return self.args.readsets.name if self.args.readsets else self.output_dir + os.sep + "run.nanuq.csv"

    @property
    def first_index(self):
        return self.args.first_index if self.args.first_index else 1

    @property
    def last_index(self):
        return self.args.last_index if self.args.last_index else 999

    @property
    def umi_index_number(self):
        if not hasattr(self, "_umi_index_number"):
            if self.args.extract_second_index:
                self._umi_index_number = 2
            elif len([rs for rs in self.readsets if (rs._umi_index_number == 1)]) > 0:
                self._umi_index_number = 1
            elif len([rs for rs in self.readsets if (rs._umi_index_number == 2)]) > 0:
                self._umi_index_number = 2
            else:
                self._umi_index_number = -1
        return self._umi_index_number

    @property
    def umi_length(self):
        if not hasattr(self, "_umi_length"):
            self._umi_length = min([rs._umi_length for rs in self.readsets])
        return self._umi_length

    @property
    def run_folder_name(self):
        return os.path.basename(self.run_dir)

    @property
    def steps(self):
        return [
            self.fastq,
            self.align,
            self.picard_mark_duplicates,
            self.metrics,
            self.blast,
            self.qc_graphs,
            self.md5,
            self.copy,
            self.end_copy_notification
        ]

    def get_eligible_readsets(self, step):
        return [readset for readset in self.readsets if readset.steps_eligibility_dict[step]]

    @property
    def read_infos(self):
        if not hasattr(self, "_read_infos"):
            self._read_infos = self.parse_run_info_file()
        return self._read_infos

    @property
    def run_name(self):
        if not hasattr(self, "_run_name"):
            if self.readsets:
                self._run_name = self.readsets[0].run
            else:
                self._run_name = self.run_id
        return self._run_name

    def get_job_list_prefix(self):
        return self.lanes_str() + "_"

    def fastq(self):
        jobs = []

        input = self.dragen_sheet_file

        # Fetch first readset if we have any readsets
        if len(self.get_eligible_readsets("fastq")) == 0:
            return jobs

        readset = (self.readsets[:1] or [None])[0]
        output_dir = self.output_dir
        if readset is not None:
            output_dir = readset.fastq_folder

        fastq_outputs = [readset.fastq1 for readset in self.readsets]
        if self.is_paired_end:
            fastq_outputs += [readset.fastq2 for readset in self.readsets]

        sample_sheet = self.dragen_sheet_file

        command = """\
rm -rf {output_dir} && mkdir -p {parent_dir} && \\
bcl-convert --bcl-input-directory {input_dir} \\
--output-directory {output_dir} \\
--strict-mode=true --sample-sheet {sample_sheet}""".format(
            input_dir=self.run_dir,
            output_dir=output_dir,
            sample_sheet=sample_sheet,
            parent_dir=os.path.abspath(os.path.join(output_dir, os.pardir))
        )
        if self.are_lanes_merged():
            command += " --no-lane-splitting=true"
        else:
            command += " --bcl-only-lane " + self.lanes_str()

        job = Job([input],
                  fastq_outputs,
                  [('DEFAULT', 'module_bcl_convert')],
                  command=command,
                  name="fastq." + self.run_name + "." + self.lanes_str(),
                  samples=self.readsets
                 )
        jobs.append(job)

        module = []
        module_httpproxy = config.param('fastq', 'module_httpproxy', required=False)
        if module_httpproxy:
            module.append(('fastq', 'module_httpproxy'))

        notification_command_start = config.param('fastq_notification_start', 'notification_command', required=False)

        if notification_command_start:
            for lane_number in self.lane_numbers:
                notification_command_start_fmt = notification_command_start.format(
                    output_dir=self.output_dir,
                    lane_number=lane_number,
                    technology=config.param('fastq', 'technology'),
                    run_dir=self.run_folder_name
                )
                # Use the same inputs and output of fastq job to send a notification each time the fastq job run
                job = Job([input], ["notificationFastqStart." + str(lane_number) + ".out"], module,
                          name="fastq_notification_start." + self.run_name + "." + str(lane_number),
                          command=notification_command_start_fmt,
                          samples=self.readsets)
                jobs.append(job)

        notification_command_end = config.param('fastq_notification_end', 'notification_command', required=False)
        if notification_command_end:
            for lane_number in self.lane_numbers:
                notification_command_end_fmt = notification_command_end.format(
                    output_dir=self.output_dir,
                    lane_number=lane_number,
                    technology=config.param('fastq', 'technology'),
                    run_dir=self.run_folder_name,
                    source=self.run_dir,
                    run_name=os.path.basename(self.run_dir)
                )
                job = Job(fastq_outputs, ["notificationFastqEnd." + str(lane_number) + ".out"], module,
                          name="fastq_notification_end." + self.run_name + "." + str(lane_number),
                          command=notification_command_end_fmt,
                          samples=self.readsets)
                jobs.append(job)

        return jobs

    def get_empty_fastq_job(self, fastq_outputs, input):
        empty_fastq_job_cmd = ""
        for output in fastq_outputs:
            empty_fastq_job_cmd += """\
      if [[ ! -e {file} ]]; then cat /dev/null | gzip > {file}; fi && \\
    
    """.format(file=output)
        empty_fastq_job_cmd += "true"
        empty_fastq_job = Job([input], fastq_outputs, command=empty_fastq_job_cmd)
        return empty_fastq_job

    def align(self):
        """
            Align the reads from the fastq file, sort the resulting .bam and create an index
            of that .bam.

            An basic aligment is performed on a sample when the `SampleRef` field of the
            Illumina sample sheet match one of the regexp in the configuration file and the
            corresponding genome (and indexes) are installed.

            `STAR` is used as a splice-junctions aware aligner when the sample
            `library_source` is `cDNA` or contains `RNA`; otherwise `BWA_mem` is used to
            align the reads.
        """
        jobs = []
        for readset in [readset for readset in self.get_eligible_readsets("align") if readset.bam]:
            align_jobs = readset.aligner.get_alignment_job(readset)
            jobs.extend(align_jobs)
        self.add_copy_job_metrics_inputs(jobs)
        return self.throttle_jobs(jobs)

    def picard_mark_duplicates(self):
        """
            Runs Picard mark duplicates on the sorted bam file.
        """
        jobs = []
        for readset in [readset for readset in self.get_eligible_readsets("picard_mark_duplicates") if (readset.bam and readset.aligner.is_run_mark_duplicate())]:
            input_file_prefix = readset.bam + '.'
            input = input_file_prefix + "bam"
            output = input_file_prefix + "dup.bam"
            metrics_file = readset.bam + ".dup.metrics"

            job = picard.mark_duplicates([input], output, metrics_file)
            job.name = "picard_mark_duplicates." + readset.name + ".dup." + readset.run + "." + self.lanes_str()
            job.samples = [readset]
            jobs.append(job)

        self.add_copy_job_metrics_inputs(jobs)
        return self.throttle_jobs(jobs)

    def metrics(self):
        """
            This step runs a series of multiple metrics collection jobs and the output bam
            from mark duplicates.

            - Picard CollectMultipleMetrics: A collection of picard metrics that runs at the
            same time to save on I/O.
                - CollectAlignmentSummaryMetrics,
                - CollectInsertSizeMetrics,
                - QualityScoreDistribution,
                - MeanQualityByCycle,
                - CollectBaseDistributionByCycle
            - BVATools DepthOfCoverage: Using the specified `BED Files` in the sample sheet,
            calculate the coverage of each target region.
            - Picard CalculateHsMetrics: Calculates a set of Hybrid Selection specific
            metrics from the BAM file. The bait and interval list is automatically created
            from the specicied `BED Files`.
        """
        jobs = []
        readsets = self.get_eligible_readsets("metrics")
        for readset in readsets:
            metrics_jobs = readset.aligner.get_fastq_metrics_jobs(readset)
            if readset.bam:
                bam_metrics_job = readset.aligner.get_metrics_jobs(readset)
                metrics_jobs.extend(bam_metrics_job)
            for job in metrics_jobs:
                job.samples = [readset]
                if job.is_optional_metric:
                    self.add_copy_job_metrics_optional_inputs([job])
                else:
                    self.add_copy_job_metrics_inputs([job])
            jobs.extend(metrics_jobs)
        return self.throttle_jobs(jobs)

    def blast(self):
        """
            Run blast on a subsample of the reads of each sample to find the 20 most
            frequent hits.

            The `runBlast.sh` tool from MUGQIC Tools is used. The number of reads to
            subsample can be configured by sample or for the whole lane. The output will be
            in the `Blast_sample` folder, under the Unaligned folder.
        """
        jobs = []
        notification_job = []

        nb_blast_to_do = config.param('blast', 'nb_blast_to_do', type="posint")
        nb_blast_to_do = max(1, (int(nb_blast_to_do) // len(self.readsets)))

        if self.args.readset_onboard_filtering:
            module = []
            module_httpproxy = config.param('fastq', 'module_httpproxy', required=False)
            if module_httpproxy:
                module.append(('fastq', 'module_httpproxy'))

            processing_start_command = config.param('blast_processing_start', 'notification_command', required=False)
            for lane_number in self.lane_numbers:
                output_file = os.path.join(self.output_dir, "notificationStart." + str(lane_number) + ".out")
                if processing_start_command:
                    processing_start_command_fmt = processing_start_command.format(
                      output_dir=self.output_dir,
                      lane_number=lane_number,
                      technology=config.param('fastq', 'technology'),
                      run_dir=self.run_folder_name
                    )
                    # Use the same inputs and output of fastq job to send a notification each time the fastq job run
                    job = Job([], [output_file], module,
                          name="blast_processing_start." + self.run_name + "." + str(lane_number),
                          command=processing_start_command_fmt,
                          samples=self.readsets)
                    notification_job.append(job)

        for readset in self.get_eligible_readsets("blast"):
            output_prefix = os.path.join(self.output_dir,
                                         "Unaligned." + readset.lane_str,
                                         "Blast_sample",
                                         readset.name + "_" + readset.sample_number + ("" if self.are_lanes_merged else ("_L00" + readset.lane_str)))
            nReadString = ".R1"
            fastq2_command = ""
            inputs = [readset.fastq1]

            if readset.fastq2:
                nReadString = ".R1R2"
                fastq2_command = " -k " + readset.fastq2
                inputs.append(readset.fastq2)

            output = output_prefix + nReadString + '.RDP.blastHit_20MF_species.txt'
            current_jobs = [Job(command="mkdir -p " + os.path.dirname(output))]

            fasta_file = output_prefix + nReadString + ".subSampled_{nb_blast_to_do}.fasta".format(nb_blast_to_do=nb_blast_to_do)
            result_file = output_prefix + nReadString + ".subSampled_{nb_blast_to_do}.blastres".format(nb_blast_to_do=nb_blast_to_do)
            command = config.param("DEFAULT", "script_path", required=False) + "runBlast.sh " + "-s " + str(nb_blast_to_do) + " -o " + output_prefix + " -f " + readset.fastq1 + fastq2_command
            if readset.is_metagenomic_library:
                command += " -c"
            current_jobs.append(Job(inputs, [output], [["blast", "module_mugqic_tools"], ["blast", "module_blast"]], command=command))

            # rRNA estimate using silva blast db, using the same subset of reads as the "normal" blast
            rrna_db = config.param('blast', 'rrna_db', required=False)
            if readset.is_rna and rrna_db:
                rrna_result_file = result_file + "Rrna"
                rrna_output = output_prefix + nReadString + ".subSampled_{nb_blast_to_do}.rrna".format(
                    nb_blast_to_do=nb_blast_to_do)
                command = """blastn -query {fasta_file} -db {db} -out {result_file} -perc_identity 80 -num_descriptions 1 -num_alignments 1""".format(
                    fasta_file=fasta_file,
                    result_file=rrna_result_file,
                    db=rrna_db
                )
                current_jobs.append(Job([], [], [["blast", "module_blast"]], command=command))

                command = """echo '{db}' > {output}""".format(
                    db=rrna_db,
                    output=rrna_output
                )
                current_jobs.append(Job([], [output], [], command=command))

                command = """grep ">" {result_file} | wc -l >> {output}""".format(
                    result_file=rrna_result_file,
                    output=rrna_output
                )
                current_jobs.append(Job([], [output], [], command=command))

                command = """grep ">" {fasta_file} | wc -l >> {output}""".format(
                    fasta_file=fasta_file,
                    output=rrna_output
                )
                current_jobs.append(Job([], [output], [], command=command))

            # merge all blast steps of the readset into one job
            job = concat_jobs(current_jobs,
                              name="blast." + readset.name + ".blast." + readset.run + "." + self.lanes_str())
            job.samples = [readset]
            jobs.append(job)
        self.add_copy_job_blast_optional_inputs(jobs)
        throttle_jobs = self.throttle_jobs(jobs, ";")
        return notification_job + throttle_jobs

    def qc_graphs(self):
        """
            Generate some QC Graphics using fastqc and multiqc

            Files are created in a 'fastqc' subfolder of the fastq directory. Examples of
            output graphic:

            - Sequence Counts
            - Per Sequence Quality Scores
            - Per Sequence GC Content
            - Per Base Sequence Content
            - Sequence Duplication Levels
            - Per Base N Content
            - Adapter Content
        """
        jobs = []
        multiqc_log_file = ""
        for readset in self.readsets:
            jobs_to_concat = []
            multiqc_output_dir = readset.fastqc_folder
            multiqc_output_dir = multiqc_output_dir.replace(self.run_dir, self.output_dir)
            if readset.steps_eligibility_dict["qc_graphs"]:
                job_name = "fastqc." + readset.name + "." + readset.run + "." + self.lanes_str()
                multiqc_output_dir = multiqc_output_dir + os.sep + readset.name
                multiqc_dir = os.path.join(multiqc_output_dir, "multiqc_data")
                file1 = readset.fastq1
                file2 = readset.fastq2
                multiqc_log_file = os.path.join(multiqc_dir, "multiqc.log")
                multiqc_output_file_list = [
                    os.path.join(multiqc_output_dir, "multiqc_report.html"),
                    os.path.join(multiqc_dir, "multiqc_data.json")
                ]
                remove_job = Job(command="rm -rf " + multiqc_output_dir)
                jobs_to_concat.append(remove_job)
                mkdir_job = Job(command="mkdir -p " + multiqc_output_dir)
                jobs_to_concat.append(mkdir_job)
                fastqc_job =  fastqc.generate_fastqc_job(file1, file2, multiqc_output_dir, multiqc_output_file_list)
                jobs_to_concat.append(fastqc_job)
                multiqc_input = multiqc_output_dir
            else:
                job_name = "multiqc." + readset.name + "." + readset.run + "." + self.lanes_str()
                multiqc_input = readset.fastqc_folder
                multiqc_output_file_list = [
                    os.path.join(multiqc_output_dir, "multiqc_report.html")
                ]
                mkdir_job = Job(command="mkdir -p " + multiqc_output_dir)
                jobs_to_concat.append(mkdir_job)

            run_script_filename = os.path.join(self.output_dir, os.path.basename(self.output_file))
            output_config = os.path.join(multiqc_output_dir, "multiqc_config.yaml")
            hostname = config.param("DEFAULT", "nanuq_host")
            generate_config_command = "python " + (config.param("DEFAULT", "script_path", required=False) + "generateMultiqcConfig.py " + run_script_filename + " " + hostname + " " + output_config)
            jobs_to_concat.append(Job(command=generate_config_command))

            module_exclude = ""
            if readset.onboard_analysis:
                secondary_analysis_folder = os.path.join(self.run_dir, readset.get_onboard_fastqc_directory())
                if os.path.exists(secondary_analysis_folder):
                    multiqc_input += " " + secondary_analysis_folder
                    module_exclude = ' --exclude "dragen_fastqc"'

            multiqc_command = "(multiqc --force -c " + output_config + module_exclude + " --cl-config 'top_modules: [\"fastqc\",\"dragen_fastqc\",\"picard\"]' --cl-config 'show_analysis_paths: false' " \
                                                                                        "--cl-config 'remove_sections: fastqc_status_checks' " \
                              + multiqc_input + " -o " + multiqc_output_dir + " && if [ ! -f " + multiqc_log_file + " ]; then exit 1; fi)"
            multiqc_job = Job(output_files=multiqc_output_file_list, command=multiqc_command, module_entries=[['DEFAULT', 'module_multiqc']])
            jobs_to_concat.append(multiqc_job)
            job = concat_jobs(jobs_to_concat)
            job.name = job_name
            job.samples = [readset]
            jobs.append(job)

        self.add_copy_job_metrics_inputs(jobs)
        return self.throttle_jobs(jobs)

    def md5(self):
        """
            Create md5 checksum files for the fastq, bam and bai using the system 'md5sum'
            util.

            One checksum file is created for each file.
        """
        jobs = []

        for readset in self.get_eligible_readsets("md5"):
            jobs_to_concatenate = []
            jobs_to_concatenate.append(Job([readset.fastq1], [readset.fastq1 + ".md5"],
                                           command="md5sum -b " + readset.fastq1 + " > " + readset.fastq1 + ".md5"))
            if readset.fastq2:
                jobs_to_concatenate.append(Job([readset.fastq2], [readset.fastq2 + ".md5"],
                                               command="md5sum -b " + readset.fastq2 + " > " + readset.fastq2 + ".md5"))

            job = concat_jobs(jobs_to_concatenate,
                              name="md5." + readset.name + ".md5." + self.run_name + "." + self.lanes_str())
            job.samples = [readset]
            jobs.append(job)

        self.add_copy_job_md5_inputs(jobs)
        return self.throttle_jobs(jobs)



    def copy(self):
        """
            Copy processed files to another place where they can be served or loaded into a
            LIMS.

            The destination folder and the command used can be set in the configuration
            file.

            An optional notification can be sent before the copy. The command used is in the configuration file.
        """
        jobs_to_concat = []
        metric_jobs_to_concat = []
        jobs = []

        # fake job to wait for optional jobs
        wait_for_blast_optional_output = self.output_dir + os.sep + "waitForBlastOptionalDone." + self.lanes_str() + ".out"
        blast_opt_job = Job(self.copy_job_blast_optional_inputs, [wait_for_blast_optional_output],
                      name="wait_for_blast_optionals." + self.run_name + "." + self.lanes_str())
        blast_opt_job.command = "touch {file}".format(file=wait_for_blast_optional_output)
        blast_opt_job.samples = self.samples
        self.add_copy_job_blast_inputs([blast_opt_job])

        jobs.append(blast_opt_job)

        wait_for_metrics_optional_output = self.output_dir + os.sep + "waitForMetricsOptionalDone." + self.lanes_str() + ".out"
        metrics_opt_job = Job(self.copy_job_metrics_optional_inputs, [wait_for_metrics_optional_output],
                      name="wait_for_metrics_optionals." + self.run_name + "." + self.lanes_str())
        metrics_opt_job.command = "touch {file}".format(file=wait_for_metrics_optional_output)
        metrics_opt_job.samples = self.samples
        self.add_copy_job_metrics_inputs([metrics_opt_job])

        jobs.append(metrics_opt_job)

        inputs_blast = self.copy_job_blast_inputs
        inputs_blast.append(wait_for_blast_optional_output)
        inputs_md5 = self.copy_job_md5_inputs
        inputs_metrics = self.copy_job_metrics_inputs
        inputs_metrics.append(wait_for_metrics_optional_output)

        module = []
        module_httpproxy = config.param('copy', 'module_httpproxy', required=False)
        if module_httpproxy:
            module.append(('copy', 'module_httpproxy'))

        fastq_lane_str = "L" + self.lanes_str().zfill(3)


        # Actual copy
        full_destination_folder = config.param('copy', 'destination_folder', type="dirpath") + os.path.basename(
            self.run_dir)
        output_fastq = full_destination_folder + os.sep + "copyFastqCompleted." + self.lanes_str() + ".out"

        excluded_files = []
        fastq_directory = set()
        fastqc_directory = set()
        analysis_directory = set()
        base_path = set()
        fastq_inputs = []
        for readset in self.get_eligible_readsets("copy"):
            if readset.bam:
                excluded_files.append(readset.bam + ".bam*")
                excluded_files.append(readset.bam + ".bai*")
            if readset.fastq1:
                base_path.add(os.path.normpath(readset.get_onboard_fastq_directory()))
                first_level_path = os.path.join(os.path.basename(os.path.dirname(readset.get_onboard_fastq_directory())), os.path.basename(readset.get_onboard_fastq_directory()))
                fastq_directory.add(first_level_path)
                sample_fastqc_dir = os.path.dirname(readset.get_onboard_fastqc_directory())
                fastqc_directory.add(sample_fastqc_dir)
                analysis_directory.add(os.path.dirname(sample_fastqc_dir))
                fastq_inputs.append(readset.fastq1)
            if readset.fastq2:
                fastq_inputs.append(readset.fastq2)

        if self.run_dir != self.output_dir:
            copy_command_run_folder = config.param('copy', 'copy_command', required=False).format(
                lane_number=self.lanes_str(),
                run_id=self.run_id,
                source=self.run_dir,
                run_name=os.path.basename(self.run_dir)
            )
            jobs_to_concat.append(Job(fastq_inputs, [output_fastq], command=copy_command_run_folder, samples=self.readsets))

        copy_command_output_folder = config.param('copy', 'copy_command', required=False).format(
            lane_number=self.lanes_str(),
            source=self.output_dir,
            run_name=os.path.basename(self.run_dir)
        )
        jobs_to_concat.append(Job(fastq_inputs, [output_fastq], command=copy_command_output_folder, samples=self.readsets))

        # Add copy of raw fastq
        base_path_ordered = []
        for base_name in base_path:
            split_path = base_name.split(os.sep)
            extending_path = ""
            for dir_name in split_path:
                extending_path = os.path.join(extending_path, dir_name)
                base_path_ordered.append(extending_path)
        include_base_dir = " ".join(
            [" --include='" + include_dir + "'" for include_dir in base_path_ordered]
        )
        include_onboard_fastq_dir = " ".join(
            [" --include='" + os.path.basename(os.path.dirname(include_dir)) + "/" + "' --include='" + include_dir + "'" for include_dir in fastq_directory]
        )
        include_qc_str = " ".join(
            [" --include='" + include_dir + "/" + "'" for include_dir in analysis_directory]
        )

        include_directories = include_base_dir + include_onboard_fastq_dir
        copy_command_raw_fastq = config.param('copy', 'copy_command_raw', required=False).format(
            source=self.run_dir,
            include=include_directories,
            run_name=os.path.basename(self.run_dir),
            destination=os.path.join(os.path.basename(self.run_dir, )),
            lane=fastq_lane_str
        )
        jobs_to_concat.append(Job(command=copy_command_raw_fastq, samples=self.readsets))
        jobs_to_concat.append(Job(command="touch " + output_fastq, samples=self.readsets))

        job = concat_jobs(jobs_to_concat, "copy." + self.run_name + "." + self.lanes_str())
        jobs.append(job)

        include_file = os.path.join(self.output_dir, "include_list_" + self.lanes_str() + ".txt")
        with open(include_file, "w") as writer:
            for line in fastqc_directory:
                writer.write(line + "\n")
                writer.write(line + "/" + "**\n")
        include_qc_str += " --include-from='" + include_file + "'"
        output_metrics = full_destination_folder + os.sep + "copyMetricsCompleted." + self.lanes_str() + ".out"
        copy_metrics_command = config.param('copy_metrics', 'copy_command', required=False).format(
            exclusion_clauses="\\\n".join(
                [" --exclude '" + excludedfile.replace(self.output_dir + os.sep, "") + "'" for excludedfile in
                 excluded_files]),
            source=self.output_dir,
            include=include_qc_str,
            run_name=os.path.basename(self.run_dir),
            lane_number=self.lanes_str(),
        )
        copy_metrics_secondary_analysis_command = config.param('copy_metrics', 'copy_secondary_analysis_command', required=False).format(
            source=self.run_dir,
            include=include_qc_str,
            run_name=os.path.basename(self.run_dir),
            lane_number=self.lanes_str()
        )

        # Notification
        notification_command = config.param('copy_metrics', 'notification_command', required=False)
        if notification_command:
            for lane_number in self.lane_numbers:
                output_notification = self.output_dir + os.sep + "notificationCopyStart." + str(lane_number) + ".out"
                notification_job = Job(inputs_metrics, [output_notification], module,
                                       name="start_copy_notification." + self.run_name + "." + str(lane_number))
                notification_job.command = notification_command.format(
                    technology=config.param('copy', 'technology'),
                    output_dir=self.output_dir,
                    run_dir=self.run_folder_name,
                    output=output_notification,
                    lane_number=lane_number
                )
                notification_job.samples = self.readsets
                metric_jobs_to_concat.append(notification_job)
        metric_jobs_to_concat.append(Job(inputs_metrics, [output_metrics], command=copy_metrics_command, samples=self.readsets))
        metric_jobs_to_concat.append(Job(command=copy_metrics_secondary_analysis_command, samples=self.readsets))
        metric_jobs_to_concat.append(Job(command="touch " + output_metrics, samples=self.readsets))

        jobs.append(concat_jobs(metric_jobs_to_concat, "copy_metrics." + self.run_name + "." + self.lanes_str()))

        output_blast = full_destination_folder + os.sep + "copyBlastCompleted." + self.lanes_str() + ".out"
        copy_blast_command = config.param('copy_blast', 'copy_command', required=False).format(
            source=self.output_dir,
            run_name=os.path.basename(self.run_dir),
            lane_number=self.lanes_str()
        )
        jobs.append(concat_jobs([Job(inputs_blast, [output_blast], command=copy_blast_command, samples=self.readsets), Job(command="touch " + output_blast, samples=self.readsets)], "copy_blast." + self.run_name + "." + self.lanes_str()))

        output_md5 = full_destination_folder + os.sep + "copyMd5Completed." + self.lanes_str() + ".out"
        copy_md5_command = config.param('copy_md5', 'copy_command', required=False).format(
            source_output=self.output_dir,
            source_original=self.run_dir,
            run_name=os.path.basename(self.run_dir),
            lane_number=self.lanes_str()
        )
        jobs.append(concat_jobs([Job(inputs_md5, [output_md5], command=copy_md5_command, samples=self.readsets), Job(command="touch " + output_md5, samples=self.readsets)], "copy_md5." + self.run_name + "." + self.lanes_str()))

        return jobs

    def end_copy_notification(self):
        """
            Send an optional notification to notify that the copy is finished.

            The command used is in the configuration file. This step is skipped when no
            command is provided.
        """
        jobs = []

        full_destination_folder = config.param('copy', 'destination_folder', type="dirpath") + os.path.basename(
            self.run_dir)
        input = [full_destination_folder + os.sep + "copyFastqCompleted." + self.lanes_str() + ".out",
                 full_destination_folder + os.sep + "copyMetricsCompleted." + self.lanes_str() + ".out",
                 full_destination_folder + os.sep + "copyBlastCompleted." + self.lanes_str() + ".out",
                 full_destination_folder + os.sep + "copyMd5Completed." + self.lanes_str() + ".out"]

        technology = config.param('end_copy_notification', 'technology')
        if config.param('end_copy_notification', 'fetch_statistics'):
            compile_statistics_input_directory = full_destination_folder
            compile_statistics_output = ["metricsStatistics_" + self.lanes_str() + ".csv"]

            jar = config.param("end_copy_notification", "jar")
            compile_statistics_job = nanuq_tools.compile_jobs_metrics_statistics(
                jar, input, compile_statistics_output, compile_statistics_input_directory, self.lanes_str(), technology
            )
            compile_statistics_job.name = "end_copy_compile_job_statistics." + self.run_name + "." + self.lanes_str()
            jobs.append(compile_statistics_job)
            input = compile_statistics_output

        output = full_destination_folder + os.sep + "notificationAssociation." + self.lanes_str() + ".out"

        notification_command = config.param('end_copy_notification', 'notification_command', required=False)

        module = []
        module_httpproxy = config.param('end_copy_notification', 'module_httpproxy', required=False)
        if module_httpproxy:
            module.append(('end_copy_notification', 'module_httpproxy'))

        if notification_command:
            output_notification_loading = full_destination_folder + os.sep + "loadingStarted." + self.lanes_str() + ".out"
            job = Job(input, [output], module, name="end_copy_notification." + self.run_name + "." + self.lanes_str())
            job.command = notification_command.format(
                technology=technology,
                output_dir=self.output_dir,
                run_name=os.path.basename(self.run_dir),
                run_dir=self.run_folder_name,
                output=output,
                output_notification_loading=output_notification_loading,
                lane_number=max(self.lane_numbers),
                file_strategy="bclconvert_onboard" if self.args.readset_onboard_filtering else "bclconvert"
            )
            job.samples = self.get_eligible_readsets("end_copy_notification")
            jobs.append(job)

        return jobs

    #
    # Utility methods
    #

    def add_copy_job_md5_inputs(self, jobs):
        for job in jobs:
            # we first remove dependencies of the current job, since we will have a dependency on that job
            self.copy_job_md5_inputs = [item for item in self.copy_job_md5_inputs if item not in job.input_files]
            self.copy_job_md5_inputs.extend(job.output_files)

    def add_copy_job_blast_inputs(self, jobs):
        for job in jobs:
            # we first remove dependencies of the current job, since we will have a dependency on that job
            self.copy_job_blast_inputs = [item for item in self.copy_job_blast_inputs if item not in job.input_files]
            self.copy_job_blast_inputs.extend(job.output_files)

    def add_copy_job_metrics_inputs(self, jobs):
        for job in jobs:
            # we first remove dependencies of the current job, since we will have a dependency on that job
            self.copy_job_metrics_inputs = [item for item in self.copy_job_metrics_inputs if item not in job.input_files]
            self.copy_job_metrics_inputs.extend(job.output_files)

    def add_copy_job_blast_optional_inputs(self, jobs):
        for job in jobs:
            # we first remove dependencies of the current job, since we will have a dependency on that job
            self.copy_job_blast_optional_inputs = [item for item in self.copy_job_blast_optional_inputs if item not in job.input_files]
            self.copy_job_blast_optional_inputs.extend(job.output_files)

    def add_copy_job_metrics_optional_inputs(self, jobs):
        for job in jobs:
            # we first remove dependencies of the current job, since we will have a dependency on that job
            self.copy_job_metrics_optional_inputs = [item for item in self.copy_job_metrics_optional_inputs if item not in job.input_files]
            self.copy_job_metrics_optional_inputs.extend(job.output_files)

    def get_sequencer_minimum_read_length(self):
        """ Returns the minimum number of cycles of a real read (not indexed). """
        return min(read.nb_cycles for read in [read for read in self.read_infos if (not read.is_index)])

    def get_mask(self):
        """ Returns a BCL2FASTQ friendly mask of the reads cycles.

            The mask is calculated using:
                - first base and last base of index;
                - the index length in the sample sheet;
                - the number of index cycles on the sequencer;
        """
        mask = ""
        index_lengths = self.get_smallest_index_length()
        index_read_count = 0
        nb_total_index_base_used = 0

        for read_info in self.read_infos:
            if len(mask) > 0:
                mask += ','
            if read_info.is_index:
                if read_info.nb_cycles >= index_lengths[index_read_count]:
                    if index_lengths[index_read_count] == 0 or self.last_index <= nb_total_index_base_used:
                        # Don't use any index bases for this read
                        mask += 'n' + str(read_info.nb_cycles)
                    else:
                        nb_n_printed = 0

                        # Ns in the beginning of the index read
                        if self.first_index > (nb_total_index_base_used + 1):
                            nb_n_printed = min(read_info.nb_cycles, self.first_index - nb_total_index_base_used - 1)
                            if nb_n_printed >= index_lengths[index_read_count]:
                                nb_n_printed = read_info.nb_cycles
                            mask += 'n' + str(nb_n_printed)

                        # Calculate the number of index bases
                        nb_index_bases_used = max(index_lengths[index_read_count] - nb_n_printed, 0)
                        nb_index_bases_used = min(self.last_index - nb_total_index_base_used - nb_n_printed,
                                                  nb_index_bases_used)
                        nb_total_index_base_used += nb_index_bases_used + min(nb_n_printed,
                                                                              index_lengths[index_read_count])
                        if nb_index_bases_used > 0:
                            mask += 'I' + str(nb_index_bases_used)

                        # Ns at the end of the index read
                        remaining_base_count = read_info.nb_cycles - nb_index_bases_used - nb_n_printed
                        if remaining_base_count > 0:
                            mask += 'n' + str(remaining_base_count)
                index_read_count += 1
            else:
                # Normal read
                mask += 'Y' + str(read_info.nb_cycles)
        return mask

    def get_smallest_index_length(self):
        """
            Returns a list (for each index read of the run) of the minimum between the number of index cycle on the
            sequencer and all the index lengths.
        """
        run_index_lengths = [r.nb_cycles for r in self.read_infos if r.is_index]  # from RunInfo

        if len(run_index_lengths) == 0 and len(self.readsets) > 1:
            raise Exception("Multiple samples on a lane, but no indexes were read from the sequencer.")

        # loop on all index reads, to compare with samples index length
        for i in range(0, len(run_index_lengths)):
            min_sample_index_length = 0
            try:
                min_sample_index_length = min(len(readset.index.split("-")[i])
                                              for readset in
                                              self.readsets
                                              if (len(readset.index.split("-")) > i and len(
                    readset.index.split("-")[i]) > 0)
                                              )
            except ValueError:
                pass  # we don't have a sample with this Ith index read, use the 0 already set

            empty_index_list = [readset for readset in self.readsets if
                                (len(readset.index.split("-")) <= i or len(readset.index.split("-")[i]) == 0)]
            if len(empty_index_list):
                # we have samples without this Ith index read, so we skip it
                min_sample_index_length = 0

            run_index_lengths[i] = min(min_sample_index_length, run_index_lengths[i])

        return run_index_lengths

    def generate_illumina_lane_sample_sheet(self):
        """ Create a sample sheet to use with the BCL2FASTQ software.

            Only the samples of the chosen lane will be in the file.
            The sample indexes are trimmed according to the mask used.
        """
        read_masks = self.mask.split(",")
        has_single_index = self.has_single_index()

        csv_headers = ["Lane", "Sample", "Index", "Index2"]
        csv_file = self.output_dir + os.sep + config.param('DEFAULT', 'casava_sample_sheet_prefix') + self.lanes_str() + ".csv"
        writer = csv.DictWriter(open(csv_file, 'wb'), delimiter=str(','), fieldnames=csv_headers, lineterminator="\n")

        writer.writeheader()

        for readset in self.readsets:
            index_to_use = ""

            if len(readset.index) > 0 and len(self.readsets) > 1:
                indexes = readset.index.split("-")
                nb_index = len(indexes)

                if has_single_index:
                    # we have a mixed of index in the sample, there are samples with 1 or 2 index,
                    # ignore the second index in the samplesheet
                    nb_index = 1

                for i in range(0, nb_index):
                    nb_ignored_leading_bases = 0
                    nb_of_index_bases = 0

                    m = re.match("(n\d+)?(I\d+)(n\d+)?", read_masks[i + 1])
                    if m:
                        if m.group(1):
                            nb_ignored_leading_bases = int(m.group(1)[1:])
                        if m.group(2):
                            nb_of_index_bases = int(m.group(2)[1:])

                    # remove ignored leading bases and trim index to smallest lane index
                    index = indexes[i][nb_ignored_leading_bases:nb_ignored_leading_bases + nb_of_index_bases]

                    if i > 0 and len(index) > 0:
                        index_to_use += "-"
                    index_to_use += index

            index_array = index_to_use.split("-")
            for lane_number in self.lane_numbers:
                csv_dict = {
                    csv_headers[0]: lane_number,
                    csv_headers[1]: readset.name,
                    csv_headers[2]: index_array[0],
                    csv_headers[3]: index_array[1] if len(index_array) > 1 else "",
                }
                writer.writerow(csv_dict)

    def has_single_index(self):
        """ Returns True when there is at least one sample on the lane that doesn't use double-indexing or we only have
            one read of indexes.
        """
        return len([readset for readset in self.readsets if ("-" not in readset.index)]) > 0 or \
            len([read for read in self.read_infos if read.is_index]) < 2

    def parse_run_info_file(self):
        """ Parse the RunInfo.xml file of the run and returns the list of RunInfoRead objects """
        reads = Xml.parse(self.run_dir + os.sep + "RunInfo.xml").getroot().find('Run').find('Reads')
        return [RunInfoRead(int(r.get("Number")), int(r.get("NumCycles")), r.get("IsIndexedRead") == "Y") for r in
                reads.iter('Read')]

    def load_readsets(self):
        """
            Download the sample sheets if required or asked for; call the load of these files and return a list of
            readsets.
        """

        if not self.args.dragen_sheet_file or self.args.force_download:
            if not os.path.exists(self.dragen_sheet_file) or self.args.force_download:
                command = config.param('DEFAULT', "fetch_dragen_sheet_command").format(
                    output_directory=self.output_dir,
                    run_dir=self.run_folder_name,
                    lane=self.lanes_str(),
                    filename=os.path.basename(self.dragen_sheet_file)
                )
                log.info(command)
                return_code = subprocess.call(command, shell=True)
                if return_code != 0:
                    raise Exception("Unable to download dragen sample sheet")

        # Nanuq readset file download
        if not self.args.readsets or self.args.force_download:
            if not os.path.exists(self.nanuq_readset_file) or self.args.force_download:
                command = config.param('DEFAULT', 'fetch_nanuq_sheet_command').format(
                    output_directory=self.output_dir,
                    run_dir=self.run_folder_name,
                    filename=self.nanuq_readset_file
                )
                return_code = subprocess.call(command, shell=True)
                if return_code != 0:
                    raise Exception("Unable to download the Nanuq readset file.")
        nb_index = 0
        for info in self.read_infos:
            if info.is_index:
                nb_index += 1

        return parse_illumina_raw_readset_files(
            self.run_dir,
            self.output_dir,
            "PAIRED_END" if self.is_paired_end else "SINGLE_END",
            self.nanuq_readset_file,
            self.dragen_sheet_file,
            None if self.are_lanes_merged() else self.lane_numbers[0],
            self.lanes_str(),
            config.param('DEFAULT', 'genomes_home', type="dirpath"),
            config.param('DEFAULT', 'skip_alignment_library_type_regex', required=False),
            config.param('DEFAULT', 'skip_alignment_for_fastq'),
            nb_index,
            [step.__name__ for step in self.steps],
            self.args.readset_onboard_filtering
        )

    def submit_jobs(self):
        super(IlluminaRunProcessing, self).submit_jobs()

    def throttle_jobs(self, jobs, job_separator="&&"):
        """ Group jobs of the same task (same name prefix) if they exceed the configured threshold number. """
        jobs_by_name = collections.OrderedDict()
        max_jobs_by_name = {}
        reply = []

        # group jobs by task (name)
        for job in jobs:
            job_name_prefix = job.name.split(".", 1)[0]
            jobs_by_name.setdefault(job_name_prefix, []).append(job)
            max_jobs_per_step = config.param(job_name_prefix, 'max_jobs_per_step', required=False, type="int")
            max_jobs_by_name[job_name_prefix] = max_jobs_per_step

        # loop on all task
        for job_name in jobs_by_name:
            current_jobs = jobs_by_name[job_name]
            current_max_jobs = max_jobs_by_name[job_name]
            if current_max_jobs and 0 < current_max_jobs < len(current_jobs):
                # we exceed the threshold, we group using 'number_task_by_job' jobs per group
                number_task_by_job = int(math.ceil(len(current_jobs) / current_max_jobs))
                merged_jobs = []
                for x in range(current_max_jobs):
                    if x * number_task_by_job < len(current_jobs):
                        merged_jobs.append(concat_jobs(
                            current_jobs[x * number_task_by_job:min((x + 1) * number_task_by_job, len(current_jobs))],
                            job_name + "." + str(x + 1) + "." + self.run_name + "." + self.lanes_str(),
                            [],
                            job_separator))
                reply.extend(merged_jobs)
            else:
                reply.extend(current_jobs)
        return reply

def distance(str1, str2):
    """ Returns the hamming distance. http://code.activestate.com/recipes/499304-hamming-distance/#c2 """
    return sum(itertools.imap(unicode.__ne__, str1, str2))




if __name__ == '__main__':
    pipeline = IlluminaRunProcessing()
