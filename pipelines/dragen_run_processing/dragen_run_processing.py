#!/usr/bin/env python

################################################################################
# Copyright (C) 2014, 2015 GenAP, McGill University and Genome Quebec Innovation Centre
#
# This file is part of MUGQIC Pipelines.
#
# MUGQIC Pipelines is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MUGQIC Pipelines is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MUGQIC Pipelines.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

# Python Standard Modules
from __future__ import print_function, division, unicode_literals, absolute_import
import os
import sys

# Append mugqic_pipelines directory to Python library path
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))

# MUGQIC Modules
from bfx.readset import *
from pipelines import common

log = logging.getLogger(__name__)


class DragenRunProcessing(common.MUGQICPipeline):
    """
    Dragen Run Processing Pipeline
    """

    def __init__(self, protocol=None):
        self._protocol = protocol
        self.copy_job_inputs = []
        self.copy_job_optional_inputs = []
        self.argparser.add_argument("-d", "--run", help="run directory", required=True, dest="run_dir")
        self.argparser.add_argument("-i",
                                    help="Dragen sample sheet. The default file is 'SampleSheet.csv' in the output folder.",
                                    type=file, required=False,
                                    dest="dragen_sheet_file")
        self.argparser.add_argument("-w", "--force-download",
                                    help="force the download of the samples sheets (default: false)",
                                    action="store_true",
                                    dest="force_download")

        super(DragenRunProcessing, self).__init__(protocol)

    @property
    def readsets(self):
        if not hasattr(self, "_readsets"):
            self._readsets = self.load_readsets()
        return self._readsets

    @property
    def run_dir(self):
        if self.args.run_dir:
            return os.path.abspath(self.args.run_dir)
        else:
            raise Exception("Error: missing '-d/--run' option!")

    @property
    def run_id(self):
        """ The run id from the run folder.
            Supports both default folder name configuration and GQ's globaly unique name convention.
        """
        if not hasattr(self, "_run_id"):
            if re.search(".*_\d+\wS\d\d[AB]", self.run_dir):
                m = re.search(".*/(\d+_[^_]+_\d+_[^_]+_(\d+)\wS.+)", self.run_dir)
                self._run_id = m.group(2)
            elif re.search(".*\d+_[^_]+_\d+_.+", self.run_dir):
                m = re.search(".*/(\d+_([^_]+_\d+)_.*)", self.run_dir)
                self._run_id = m.group(2)
            else:
                log.warn("Unsupported folder name: " + self.run_dir)

        return self._run_id

    @property
    def dragen_sheet_file(self):
        return self.args.dragen_sheet_file.name \
            if self.args.dragen_sheet_file else self.run_dir + os.sep + "SampleSheet.csv"

    @property
    def run_folder_name(self):
        return os.path.basename(self.run_dir)

    @property
    def steps(self):
        return [
            self.fastq,
            self.align,
            self.rename_md5,
            self.qc,
            self.copy,
            self.association
        ]

    def get_fastq_output_dir(self):
        return self.run_dir + os.sep + "Dragen.fastq"

    def get_fastq_list(self):
        return self.get_fastq_output_dir() + os.sep + "Reports" + os.sep + "fastq_list.csv"

    def get_align_output_dir(self):
        base_output_dir = config.param('DEFAULT', 'align_base_output_dir', required=False)
        if base_output_dir != "":
            return base_output_dir + os.sep + os.path.basename(self.run_dir) + os.sep + "Dragen.merged"
        else:
            return self.run_dir + os.sep + "Dragen.merged"

    def get_dragen_fastq_time_metrics_file(self):
        return self.get_fastq_output_dir() + os.sep + "dragen.time_metrics.csv"

    def fastq(self):

        jobs = []
        current_jobs = []
        input = self.dragen_sheet_file
        output_dir = self.get_fastq_output_dir()
        strict_mode = config.param('fastq', 'strict_mode')
        no_lane_splitting = config.param("fastq", "no_lane_splitting")
        bcl_conversion_only = config.param("fastq", "bcl_conversion_only")
        notification_command_start = config.param('fastq_notification_start', 'notification_command')
        notification_command_start = notification_command_start.format(
            output_dir=self.run_dir,
            lane_number=4,
            technology=config.param('DEFAULT', 'technology'),
            run_dir=self.run_folder_name
        )
        current_jobs.append(Job([], ["notificationFastqStart.4.out"], command=notification_command_start))
        command = ("dragen -f --bcl-conversion-only={bcl_conversion_only} --bcl-input-dir=\"{run_dir}\" --sample-sheet=\"{sample_sheet}\" "
                    "--output-directory=\"{output_dir}\" --strict-mode={strict_mode} --no-lane-splitting={no_lane_splitting} {other_options}").format(
            bcl_conversion_only=bcl_conversion_only,
            run_dir=self.run_dir,
            sample_sheet=self.dragen_sheet_file,
            output_dir=output_dir,
            strict_mode=strict_mode,
            no_lane_splitting=no_lane_splitting,
            other_options=config.param('fastq', 'other_options', required=False)
        )
        fastq_job_outputs = []
        for readset in self.readsets:
            fastq_job_outputs.append(readset.fastq1)
            fastq_job_outputs.append(readset.fastq2)
        fastq_job_outputs.append(self.get_fastq_list())
        fastq_job_outputs.append(self.get_dragen_fastq_time_metrics_file())

        job = Job([input], fastq_job_outputs, command=command)
        current_jobs.append(job)
        notification_command_end = config.param('fastq_notification_end', 'notification_command')
        notification_command_end = notification_command_end.format(
            output_dir=self.run_dir,
            lane_number=4,
            technology=config.param('DEFAULT', 'technology'),
            run_dir=self.run_folder_name
        )
        current_jobs.append(Job([], ["notificationFastqEnd.4.out"], command=notification_command_end))
        job = concat_jobs(current_jobs, "fastq")
        job.samples = self.samples
        jobs.append(job)
        self.add_copy_job_inputs(jobs)
        return jobs

    def align(self):
        jobs = []
        current_jobs = []
        fastq_list = self.get_fastq_list()
        align_output = []

        notification_command_start = config.param('alignment_notification_start', 'notification_command')
        notification_command_start = notification_command_start.format(
            output_dir=self.run_dir,
            lane_number=4,
            technology=config.param('DEFAULT', 'technology'),
            run_id=self.run_id
        )
        job = Job([self.get_dragen_fastq_time_metrics_file()], ["notificationAlignStart.4.out"],
                  name="notification_align_start." + self.run_id + ".4",
                  command=notification_command_start,
                  samples=self.samples)
        current_jobs.append(job)

        for readset in self.readsets:
            output_dir = self.get_align_output_dir() + os.sep + readset.name

            command = (
                "mkdir -p {output_dir} && dragen -f --fastq-list=\"{fastq_list}\" "
                "--fastq-list-sample-id=\"{fastq_list_sample_id}\" "
                "--intermediate-results-dir=\"{intermediate_results_dir}\" "
                "--output-directory=\"{output_dir}\" "
                "--ref-dir=\"{ref_dir}\" "
                "--enable-map-align-output={enable_map_align_output} "
                "--enable-duplicate-marking={enable_duplicate_marking} "
                "--output-format={output_format} "
                "--enable-sort={enable_sort} "
                "--enable-variant-caller={enable_variant_caller} "
                "--vc-enable-roh={vc_enable_roh} "
                "--vc-min-base-qual={vc_min_base_qual} "
                "--vc-min-read-qual={vc_min_read_qual} "
                "--vc-emit-ref-confidence={vc_emit_ref_confidence} "
                "--enable-sv={enable_sv} "
                "--output-file-prefix=\"{output_file_prefix}\" "
                "--enable-cnv={enable_cnv} "
                "--cnv-enable-plots={cnv_enable_plots} "
                "--cnv-enable-tracks={cnv_enable_tracks} "
                "--cnv-enable-gcbias-correction={cnv_enable_gcbias_correction} "
                "--cnv-enable-self-normalization={cnv_enable_self_normalization}"
            ).format(
                fastq_list=fastq_list,
                fastq_list_sample_id=readset.name,
                intermediate_results_dir=config.param("align", "intermediate_results_dir"),
                output_dir=output_dir,
                ref_dir=config.param("align", "ref_dir"),
                enable_map_align_output=config.param("align", "enable_map_align_output"),
                enable_duplicate_marking=config.param("align", "enable_duplicate_marking"),
                output_format=config.param("align", "output_format"),
                enable_sort=config.param("align", "enable_sort"),
                enable_variant_caller=config.param("align", "enable_variant_caller"),
                vc_enable_roh=config.param("align", "vc_enable_roh"),
                vc_min_base_qual=config.param("align", "vc_min_base_qual"),
                vc_min_read_qual=config.param("align", "vc_min_read_qual"),
                vc_emit_ref_confidence=config.param("align", "vc_emit_ref_confidence"),
                enable_sv=config.param("align", "enable_sv"),
                output_file_prefix=readset.name,
                enable_cnv=config.param("align", "enable_cnv"),
                cnv_enable_plots=config.param("align", "cnv_enable_plot"),
                cnv_enable_tracks=config.param("align", "cnv_enable_tracks"),
                cnv_enable_gcbias_correction=config.param("align", "cnv_enable_gcbias_correction"),
                cnv_enable_self_normalization=config.param("align", "cnv_enable_self_normalization")
            )
            cnv_output = output_dir + os.sep + readset.name + ".cnv.vcf.gz"
            cram_output = self.get_readset_cram_file(readset)
            gvcf_output = output_dir + os.sep + readset.name + ".hard-filtered.gvcf.gz"
            sv_output = output_dir + os.sep + readset.name + ".sv.vcf.gz"
            ploidy_output = output_dir + os.sep + readset.name + ".ploidy.vcf.gz"
            output = [cnv_output, cram_output, gvcf_output, sv_output, ploidy_output]
            align_output.extend(output)
            job = Job([fastq_list, self.get_dragen_fastq_time_metrics_file()], output, command=command, samples=[readset.sample], name="alignment_" + readset.name)
            current_jobs.append(job)

        job = Job(output_files=[self.get_align_done_file()], command="touch " + self.get_align_done_file(), samples=self.samples, name="alignment_done")
        current_jobs.append(job)
        notification_command_end = config.param('alignment_notification_end', 'notification_command')
        notification_command_end = notification_command_end.format(
            output_dir=self.run_dir,
            lane_number=4,
            technology=config.param('DEFAULT', 'technology'),
            run_id=self.run_id
        )
        job = Job(align_output, ["notificationAlignEnd.4.out"],
                  name="notification_align_end." + self.run_id + ".4",
                  command=notification_command_end,
                  samples=self.samples)
        current_jobs.append(job)
        job = concat_jobs(current_jobs, "align")
        job.samples = self.samples
        jobs.append(job)
        self.add_copy_job_inputs(jobs)
        return jobs

    def rename_md5(self):
        jobs = []
        for readset in self.readsets:
            input_dir = self.get_align_output_dir() + os.sep + readset.name
            cnv = input_dir + os.sep + readset.name + ".cnv.vcf.gz"
            gvcf = input_dir + os.sep + readset.name + ".hard-filtered.gvcf.gz"
            cram = self.get_readset_cram_file(readset)
            ploidy = input_dir + os.sep + readset.name + ".ploidy.vcf.gz"
            inputs = [cnv, gvcf, cram, ploidy]
            rename_command = config.param("rename_md5", "rename_command").format(input_dir=input_dir, ext="md5sum", new_ext="md5")
            job = Job(input_files=inputs, command=rename_command, name="rename_md5")
            jobs.append(job)

        job = Job(output_files=[self.get_rename_md5_file()], command="touch " + self.get_rename_md5_file(), samples=self.samples, name="rename_md5_done")
        jobs.append(job)
        self.add_copy_job_inputs(jobs)
        return jobs

    def qc(self):
        jobs = []
        verify_bam_id_command_arr = []
        rename_command_arr = []
        inputs = []
        outputs = []
        outputs_tsv_list = []
        max_per_job_call = int(config.param("qc", "max_per_job_call"))
        for i in range(0, len(self.readsets), max_per_job_call):
            readset_sublist = self.readsets[i:max_per_job_call]
            for readset in readset_sublist:

                input = self.get_readset_cram_file(readset)
                inputs.append(input)
                output_prefix = self.get_align_output_dir() + os.sep + readset.name + os.sep + readset.name + ".metrics.verifyBamId"
                output_tsv = output_prefix + ".tsv"
                outputs_tsv_list.append(output_tsv)
                output = self.get_align_output_dir() + os.sep + readset.name + os.sep + readset.name + ".metrics.verifyBamId" + ".selfSM"
                outputs.append(output)

                verify_bam_id_command_arr.append(config.param("qc", "verify_bam_id_command").format(
                    cram_file=input,
                    output_file=output_prefix
                ))

                rename_command_arr.append(
                    "cut -f2- {input} > {output}".format(input=output, output=output_tsv)
                )

            rename_command_arr.append("wait")
            verify_bam_id_command_arr.append("wait")

            jobs.append(Job(inputs, outputs, command=' \\\n& '.join(verify_bam_id_command_arr), name="verify_bam_ids_" + str(i)))
            jobs.append(Job(outputs, outputs_tsv_list, command=' \\\n& '.join(rename_command_arr), name="rename_selfSM_" + str(i)))
        return jobs

    def copy(self):
        jobs = []
        current_jobs = []
        copy_start_notification = config.param('copy', 'copy_notification_start').format(
            output_dir=self.run_dir,
            lane_number=4,
            technology=config.param('DEFAULT', 'technology'),
            run_dir=self.run_folder_name
        )
        input = self.get_align_done_file()
        output = self.run_dir + os.sep + "copyStart.4.out"
        job = Job([input], [output], command=copy_start_notification)
        current_jobs.append(job)

        # base run folder copy
        rsync_command = config.param('copy', 'rsync_command').format(
            beluga_output=config.param('DEFAULT', 'beluga_output') + os.sep + os.path.basename(self.run_dir),
            run_dir=self.run_dir,
        )
        input = self.get_align_done_file()
        job = Job([input], [], command=rsync_command)
        current_jobs.append(job)

        # staging align files
        base_output_dir = config.param('DEFAULT', 'align_base_output_dir', required=False)
        if base_output_dir != "":
            rsync_command_align = config.param('copy', 'rsync_command').format(
                beluga_output=config.param('DEFAULT', 'beluga_output') + os.sep + os.path.basename(self.run_dir),
                run_dir=os.path.dirname(self.get_align_output_dir())
            )
            job = Job([input], [], command=rsync_command_align)
            current_jobs.append(job)

        # copy done flag
        output = self.get_copy_done_file()
        job = Job([], [output], command="touch " + self.get_copy_done_file())
        current_jobs.append(job)

        job = concat_jobs(current_jobs, "copy")
        job.samples = self.samples
        jobs.append(job)
        return jobs

    def association(self):
        jobs = []
        current_jobs = []
        input = self.get_copy_done_file()
        output = self.get_association_done_file()
        association_command = config.param("association", "association_command").format(
            run_id=self.run_id,
            technology=config.param('DEFAULT', 'technology'),
            lane_number=4,
        )
        job = Job([input], [], command=association_command)
        current_jobs.append(job)
        job = Job([], [output], command="touch " + self.get_association_done_file())
        current_jobs.append(job)
        job = concat_jobs(current_jobs, "association")
        job.samples = self.samples
        jobs.append(job)

        # remove align directory on staging fs; waiting after the association to make sure we don't need the files
        base_output_dir = config.param('DEFAULT', 'align_base_output_dir', required=False)
        if base_output_dir != "":
            cleaning_job = Job([self.get_association_done_file()], [output], command="rm -r " + os.path.dirname(self.get_align_output_dir()))
            cleaning_job.samples = self.samples
            cleaning_job.name = "clean_align_dir"
            jobs.append(cleaning_job)

        return jobs

    def load_readsets(self):
        if not self.args.dragen_sheet_file or self.args.force_download:
            if not os.path.exists(self.dragen_sheet_file) or self.args.force_download:
                command = config.param('DEFAULT', 'fetch_dragen_sample_sheet_command').format(
                    run_id=self.run_id,
                    filename=self.dragen_sheet_file
                )
                log.info(command)
                return_code = subprocess.call(command, shell=True)
                if return_code != 0:
                    raise Exception("Unable to download the Dragen Sample Sheet.")
        return parse_dragen_sample_sheet(self.get_fastq_output_dir(), self.dragen_sheet_file)

    def add_copy_job_inputs(self, jobs):
        for job in jobs:
            # we first remove dependencies of the current job, since we will have a dependency on that job
            self.copy_job_inputs = [item for item in self.copy_job_inputs if item not in job.input_files]
            self.copy_job_inputs.extend(job.output_files)

    def get_align_done_file(self):
        return self.run_dir + os.sep + "align.done"

    def get_copy_done_file(self):
        return self.run_dir + os.sep + "copy.done"

    def get_association_done_file(self):
        return self.run_dir + os.sep + "association.done"

    def get_rename_md5_file(self):
        return self.run_dir + os.sep + "renameMd5.done"

    def get_readset_cram_file(self, readset):
        return self.get_align_output_dir() + os.sep + readset.name + os.sep + readset.name + ".cram"

if __name__ == '__main__':
    pipeline = DragenRunProcessing()
