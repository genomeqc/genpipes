#!/usr/bin/env python

################################################################################
# Copyright (C) 2014, 2015 GenAP, McGill University and Genome Quebec Innovation Centre
#
# This file is part of MUGQIC Pipelines.
#
# MUGQIC Pipelines is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MUGQIC Pipelines is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MUGQIC Pipelines.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

# Python Standard Modules
from __future__ import print_function, division, unicode_literals, absolute_import
import math
import os
import sys
import xml.etree.ElementTree as Xml

# Append mugqic_pipelines directory to Python library path
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))

# MUGQIC Modules
from bfx.readset import *
from pipelines import common


log = logging.getLogger(__name__)



class IlluminaSecondaryAnalysis(common.MUGQICPipeline):

    def __init__(self, protocol=None):
        self._protocol = protocol
        self.argparser.add_argument("-d", "--run", help="run directory", required=False, dest="run_dir")
        self.argparser.add_argument("-r", "--readsets",
                                    help="nanuq readset file. The default file is 'run.nanuq.csv' in the output folder. Will be automatically downloaded if not present.",
                                    type=file, required=False)
        self.argparser.add_argument("-i",
                                    help="illumina dragen sample sheet. The default file is 'DragenSampleSheet.nanuq.csv' in the output folder. Will be automatically downloaded if not present",
                                    type=file, required=False,
                                    dest="dragen_sheet_file")
        self.argparser.add_argument("-r", "--readsets",
                                    help="nanuq readset file. The default file is 'run.nanuq.csv' in the output folder. Will be automatically downloaded if not present.",
                                    type=file, required=False)
        self.argparser.add_argument("-w", "--force-download",
                                    help="force the download of the samples sheets (default: false)",
                                    action="store_true",
                                    dest="force_download")

        super(IlluminaSecondaryAnalysis, self).__init__(protocol)

    @property
    def readsets(self):
        if not hasattr(self, "_readsets"):
            self._readsets = self.load_readsets()
        return self._readsets

    @property
    def run_dir(self):
        if self.args.run_dir:
            return os.path.abspath(self.args.run_dir)
        else:
            raise Exception("Error: missing '-d/--run' option!")

    @property
    def run_id(self):
        """ The run id from the run folder.
            Supports both default folder name configuration and GQ's globaly unique name convention.
        """
        if not hasattr(self, "_run_id"):
            if re.search(".*_\d+\wS\d\d[AB]", self.run_dir):
                m = re.search(".*/(\d+_[^_]+_\d+_[^_]+_(\d+)\wS.+)", self.run_dir)
                self._run_id = m.group(2)
            elif re.search(".*\d+_[^_]+_\d+_.+", self.run_dir):
                m = re.search(".*/(\d+_([^_]+_\d+)_.*)", self.run_dir)
                self._run_id = m.group(2)
            else:
                log.warn("Unsupported folder name: " + self.run_dir)

        return self._run_id

    @property
    def dragen_sheet_file(self):
        return self.args.dragen_sheet_file.name \
            if self.args.dragen_sheet_file else self.output_dir + os.sep + "SampleSheetDragen.nanuq.csv"

    @property
    def run_folder_name(self):
        return os.path.basename(self.run_dir)

    @property
    def is_paired_end(self):
        if not hasattr(self, "_is_paired_end"):
            self._is_paired_end = len([read_info for read_info in self.read_infos if (not read_info.is_index)]) > 1
        return self._is_paired_end

    @property
    def read_infos(self):
        if not hasattr(self, "_read_infos"):
            self._read_infos = self.parse_run_info_file()
        return self._read_infos

    @property
    def notification_module(self):
        if not hasattr(self, "_notification_module"):
            self._notification_module = []
            module_httpproxy = config.param('default', 'module_httpproxy', required=False)
            if module_httpproxy:
                self._notification_module.append(('default', 'module_httpproxy'))
        return self._notification_module

    @property
    def nanuq_readset_file(self):
        return self.args.readsets.name if self.args.readsets else self.output_dir + os.sep + "run.nanuq.csv"

    def parse_run_info_file(self):
        """ Parse the RunInfo.xml file of the run and returns the list of RunInfoRead objects """
        reads = Xml.parse(self.run_dir + os.sep + "RunInfo.xml").getroot().find('Run').find('Reads')
        return [RunInfoRead(int(r.get("Number")), int(r.get("NumCycles")), r.get("IsIndexedRead") == "Y") for r in
                reads.iter('Read')]

    @property
    def steps(self):
        return [
            self.compress,
            self.association
        ]

    def compress(self):
        jobs = []

        # Compression job
        analysis_number_list = self.get_analysis_nb_list()
        above_structure = self.get_analysis_folder()
        for readset in self.readsets:
            files_to_regroup = []
            dragen_versions = ""

            for analysis_number in analysis_number_list:
                # For each analysis folder in decreasing create time
                dragen_versions = self.parse_sample_sheet_for_dragen_versions(
                    self.get_analysis_sample_sheet(readset.secondary_analysis_type, analysis_number),
                    readset.secondary_analysis_type
                )

                for analysis_file in readset.get_secondary_analysis_files():
                    # For each analysis files, if exists add them to a list to be zipped
                    sub_structure = readset.get_analysis_type_subfolder_structure()
                    path = os.path.join(above_structure, analysis_number, sub_structure)
                    file = os.path.join(path, analysis_file)
                    md5_file = file + ".md5sum"
                    if os.path.exists(file):
                        files_to_regroup.append(file)
                    if os.path.exists(md5_file):
                        files_to_regroup.append(md5_file)
                if files_to_regroup:
                    # If we found files to zip don't descend further in the analysis number structure
                    # in other words, if we find files in analysis folder 2, don't check analysis folder 1
                    break
            if files_to_regroup:
                destination_path = os.path.join(config.param('compression', 'analysis_drop_path'), readset.secondary_analysis_zip)
                zip_command = "zip -0 -j " + destination_path

                for file in files_to_regroup:
                    zip_command += " " + file

                if dragen_versions:
                    zip_command += "; echo " + dragen_versions + " > tmp.txt; zip -z " + destination_path + " < tmp.txt; rm tmp.txt"

                jobs.append(Job(
                    input_files=files_to_regroup,
                    command=zip_command,
                    output_files=[destination_path],
                    samples=[readset],
                    name="compression" + "." + readset.run + "." + readset.secondary_analysis_type + "." + readset.name
                ))
        return self.throttle_jobs(jobs)

    def association(self):
        jobs = []

        inputs = [os.path.join(config.param('compression', 'analysis_drop_path'), readset.secondary_analysis_zip) for readset in self.readsets]
        if inputs:
            output = os.path.join(self.output_dir, "secondaryAnalysisDone.out")
            association_command = config.param('association_secondary_analysis', 'association_command', required=False)
            association_command = association_command.format(
                tech=config.param('default', 'technology'),
                run_directory=self.run_folder_name,
                output=output
            )
            job = Job(
                command=association_command,
                input_files=inputs,
                output_files=[output],
                samples=self.readsets,
                name="association_secondary_analysis" + "." + self.run_id
            )
            jobs.append(job)
        return jobs

    def throttle_jobs(self, jobs, job_separator="&&"):
        """ Group jobs of the same task (same name prefix) if they exceed the configured threshold number. """
        max_jobs_per_step = config.param('default', 'max_jobs_per_step', required=False, type="int")
        jobs_by_name = collections.OrderedDict()
        reply = []

        # group jobs by task (name)
        for job in jobs:
            jobs_by_name.setdefault(job.name.split(".", 1)[0], []).append(job)

        # loop on all task
        for job_name in jobs_by_name:
            current_jobs = jobs_by_name[job_name]
            if max_jobs_per_step and 0 < max_jobs_per_step < len(current_jobs):
                # we exceed the threshold, we group using 'number_task_by_job' jobs per group
                number_task_by_job = int(math.ceil(len(current_jobs) / max_jobs_per_step))
                merged_jobs = []
                for x in range(max_jobs_per_step):
                    if x * number_task_by_job < len(current_jobs):
                        merged_jobs.append(concat_jobs(
                            current_jobs[x * number_task_by_job:min((x + 1) * number_task_by_job, len(current_jobs))],
                            job_name + "." + str(x + 1) + "." + self.run_id,
                            [],
                            job_separator))
                reply.extend(merged_jobs)
            else:
                reply.extend(current_jobs)
        return reply

    def get_analysis_sample_sheet(self, analysis_type, analysis_number):
        return os.path.join(self.get_analysis_folder(), analysis_number, "Data", analysis_type, "SampleSheet.csv")

    def get_zip_filename(self, readset, analysis_type):
        return self.run_id + "_" + analysis_type + "_" + readset.name + ".gz"

    def get_analysis_nb_list(self):
        folder = self.get_analysis_folder()
        analysis_number_directory = os.listdir(folder)
        analysis_number_directory_fullpath = [os.path.join(folder, analysis_number) for analysis_number in analysis_number_directory]
        return sorted(analysis_number_directory_fullpath, key=os.path.getmtime, reverse=True)

    def get_analysis_folder(self):
        return os.path.join(self.output_dir, "Analysis")

    def load_readsets(self):
        """
            Download the sample sheets if required or asked for; call the load of these files and return a list of
            readsets.
        """

        if not self.args.dragen_sheet_file or self.args.force_download:
            if not os.path.exists(self.dragen_sheet_file) or self.args.force_download:
                command = config.param('compression', "fetch_dragen_sheet_command").format(
                    output_directory=self.output_dir,
                    run_dir=self.run_folder_name,
                    filename=os.path.basename(self.dragen_sheet_file)
                )
                log.info(command)
                return_code = subprocess.call(command, shell=True)
                if return_code != 0:
                    raise Exception("Unable to download dragen sample sheet")

        if not self.args.readsets or self.args.force_download:
            if not os.path.exists(self.nanuq_readset_file) or self.args.force_download:
                command = config.param('DEFAULT', 'fetch_nanuq_sheet_command').format(
                    output_directory=self.output_dir,
                    run_dir=self.run_folder_name,
                    filename=self.nanuq_readset_file
                )
                return_code = subprocess.call(command, shell=True)
                if return_code != 0:
                    raise Exception("Unable to download the Nanuq readset file.")

        return parse_illumina_raw_readset_file_for_secondary_analysis(
            "PAIRED_END" if self.is_paired_end else "SINGLE_END",
            self.nanuq_readset_file,
            self.dragen_sheet_file,
        )

    def parse_sample_sheet_for_dragen_versions(self, path, analysis_type):
        analysis_type_section_regex = re.compile("^\\[" + analysis_type + "_Settings\\]$")
        software_regex = re.compile("SoftwareVersion")
        app_regex = re.compile("AppVersion")
        section_found = 0

        software_version = ""
        software_found = 0
        app_version = ""
        app_found = 0

        if not os.path.exists(path):
            return ""

        file = open(path, 'rb')
        for line in file:
            if analysis_type_section_regex.match(line):
                section_found = 1
                continue
            if not section_found:
                continue
            split_line = line.split(",")
            if software_regex.match(split_line[0]):
                software_version = "".join(split_line[1].splitlines())
                software_found = 1
            if app_regex.match(split_line[0]):
                app_version = "".join(split_line[1].splitlines())
                app_found = 1
            if software_found and app_found:
                break
        return software_version + "," + app_version

class RunInfoRead(object):
    """ Model of a read from the Illumina sequencer.
        Those attributes can be found in the RunInfo.xml file.
    """

    def __init__(self, number, nb_cycles, is_index):
        self._number = number
        self._nb_cycles = nb_cycles
        self._is_index = is_index

    @property
    def number(self):
        return self._number

    @property
    def nb_cycles(self):
        return self._nb_cycles

    @property
    def is_index(self):
        return self._is_index

if __name__ == '__main__':
    pipeline = IlluminaSecondaryAnalysis()
