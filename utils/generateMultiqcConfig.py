import argparse
import os

parser = argparse.ArgumentParser(
    prog="generateMultiqcConfig",
    description="Extract module load program and their version from genpipes scripts and write to output the config in a yaml format",
)

#Genpipes script
parser.add_argument('filename')
parser.add_argument('hostname')
parser.add_argument('output')
args = parser.parse_args()
modules = {}
if not os.path.exists(args.filename):
    print("args.filename does not exists")
    exit()


with open(args.filename) as reader:
    lines = reader.readlines()
    for line in lines:
        if "module load" in line:
            split_line = line.split()
            sub_modules = [seg for seg in split_line if "/" in seg and "StdEnv" not in seg]
            for mod in sub_modules:
                split_module = mod.split("/")
                module_name = split_module[-2]
                version = split_module[-1]
                if module_name in modules:
                    modules[module_name].update({version})
                else:
                    modules[module_name] = {version.strip()}

# Missing bcl-convert software version. Extract bcl convert version from dragen-sample-sheet
if not "bcl-convert" in modules:
    onboard_bclconvert_software_version = ""
    run_dir = os.path.dirname(args.filename)
    # Assumption on the following: SampleSheet.csv is in the same folder as the script.
    # This should be always true
    onboard_sample_sheet = os.path.join(run_dir, "SampleSheet.csv")
    if (os.path.exists(onboard_sample_sheet)):
        with open(onboard_sample_sheet) as reader:
            lines = reader.readlines()
            for line in lines:
                split_line = line.split(",")
                if split_line and "SoftwareVersion" == split_line[0] and len(split_line) > 1:
                    onboard_bclconvert_software_version = split_line[1].strip()
                    break
        if onboard_bclconvert_software_version:
            modules["Dragen_Bcl-Convert"] = {onboard_bclconvert_software_version}

with open(args.output, "w") as writer:
    # Start with the modification to the header
    logo_path = os.path.join(os.path.dirname(__file__), "logo1.gif")
    writer.write("custom_logo: " + "\"{logo_path}\"\n".format(logo_path=logo_path))
    if args.hostname:
        writer.write("custom_logo_url: " + '"' + args.hostname + '"\n')
    writer.write("custom_logo_title: " + "\"Nanuq\"\n")
    writer.write("intro_text: " + "\"Aggregate report\"\n")
    writer.write("software_versions:\n")
    for module in modules:
        module = module.replace('"','').strip()
        # fastqc version is already in the report
        if module == "fastqc":
            continue
        writer.write("  " + module +":")
        if len(modules[module]) == 1:
            writer.write(' "{version}"\n'.format(version=modules[module].pop()))
        else:
            writer.write("\n")
            for version in modules[module]:
                writer.write('    -"' + version + "\"\n")