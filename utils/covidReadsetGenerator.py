#!/usr/bin/env python

import os
import sys
import re

run_dir = sys.argv[1]
run_name = sys.argv[2]
outfile_name = sys.argv[3]

lib_type = ""
run_type = ""
adapter1 = ""
adapter2 = ""

with open (run_dir+"/run.nanuq.csv") as run_file:
    for i, line in enumerate(run_file):
        if i == 2:
            run_info = re.split(r',', line)
            lib_type = run_info[19]
            run_type = run_info[9]
            adapter1 = run_info[24]
            adapter2 = run_info[25]
            break

with open (outfile_name, 'w') as outfile:
    outfile.write("Sample\tReadset\tLibraryType\tRunType\tRun\tLane\tAdapter1\tAdapter2\tQualityOffset\tBED\tFASTQ1\tFASTQ2\tBAM\n")
    for R1_file in os.popen('find '+run_dir+'/Unaligned* -name "*COVID*_R1_*fastq.gz"'):
        R1_file = R1_file.strip()
        R1_file = R1_file.replace(run_dir+"/Unaligned.merged/Project_nanuq/","")
        R2_file = R1_file.replace("_R1_","_R2_")
        sampleRegex = re.compile(r'Sample_[^/]+_COVID')
        sample_name = sampleRegex.search(R1_file)
        sampleID = sample_name.group()
        sampleID = sampleID.replace("Sample_","").replace("_COVID","")
        outfile.write(sampleID+"\t"+sampleID+"\t"+lib_type+"\t"+run_type+"\t"+run_name+"\t1,2,3,4\t"+adapter1+"\t"+adapter2+"\t33\t\t"+R1_file+"\t"+R2_file+"\t\n")

