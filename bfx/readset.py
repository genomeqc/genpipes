#!/usr/bin/env python

################################################################################
# Copyright (C) 2014, 2015 GenAP, McGill University and Genome Quebec Innovation Centre
#
# This file is part of MUGQIC Pipelines.
#
# MUGQIC Pipelines is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MUGQIC Pipelines is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MUGQIC Pipelines.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

# Python Standard Modules
from collections import namedtuple
import csv
import logging
import os
import re
import datetime

# MUGQIC Modules
from run_processing_aligner import *
from sample import *
from core.config import *

log = logging.getLogger(__name__)

class Readset(object):

    def __init__(self, name):
        if re.search("^[\w-][\w.-]*$", name):
            self._name = name
        else:
            raise Exception("Error: readset name \"" + name +
                "\" is invalid (should match [a-zA-Z0-9_-][a-zA-Z0-9_.-]*)!")

    @property
    def name(self):
        return self._name

    @property
    def sample(self):
        return self._sample


class IlluminaReadset(Readset):

    def __init__(self, name, run_type):
        super(IlluminaReadset, self).__init__(name)

        if run_type in ("PAIRED_END", "SINGLE_END"):
            self._run_type = run_type
        else:
            raise Exception("Error: readset run_type \"" + run_type +
                "\" is invalid (should be \"PAIRED_END\" or \"SINGLE_END\")!")

        self.fastq1 = None
        self.fastq2 = None

    @property
    def run_type(self):
        return self._run_type

    @property
    def bam(self):
        if not hasattr(self, "_bam"):
            return None
        else:
            return self._bam

    @property
    def library(self):
        return self._library

    @property
    def run(self):
        return self._run

    @property
    def lane(self):
        return self._lane

    @property
    def adapter1(self):
        return self._adapter1

    @property
    def adapter2(self):
        return self._adapter2

    @property
    def quality_offset(self):
        return self._quality_offset

    @property
    def beds(self):
        return self._beds
    @property
    def is_10x_dual_index(self):
        return self._is_10x_dual_index

def parse_illumina_readset_file(illumina_readset_file):
    readsets = []
    samples = []

    log.info("Parse Illumina readset file " + illumina_readset_file + " ...")
    readset_csv = csv.DictReader(open(illumina_readset_file, 'rb'), delimiter='\t')
    for line in readset_csv:
        sample_name = line['Sample']
        sample_names = [sample.name for sample in samples]
        if sample_name in sample_names:
            # Sample already exists
            sample = samples[sample_names.index(sample_name)]
        else:
            # Create new sample
            sample = Sample(sample_name)
            samples.append(sample)

        # Create readset and add it to sample
        readset = IlluminaReadset(line['Readset'], line['RunType'])

        # Readset file paths are either absolute or relative to the readset file
        # Convert them to absolute paths
        for format in ("BAM", "FASTQ1", "FASTQ2"):
            if line.get(format, None):
                line[format] = os.path.expandvars(line[format])
                if not os.path.isabs(line[format]):
                    line[format] = os.path.dirname(os.path.abspath(os.path.expandvars(illumina_readset_file))) + os.sep + line[format]
                line[format] = os.path.normpath(line[format])

        readset._bam = line.get('BAM', None)
        readset.fastq1 = line.get('FASTQ1', None)
        readset.fastq2 = line.get('FASTQ2', None)
        readset._library = line.get('Library', None)
        readset._run = line.get('Run', None)
        readset._lane = line.get('Lane', None)
        readset._adapter1 = line.get('Adapter1', None)
        readset._adapter2 = line.get('Adapter2', None)
        readset._quality_offset = int(line['QualityOffset']) if line.get('QualityOffset', None) else None
        readset._beds = line['BED'].split(";") if line.get('BED', None) else []

        readsets.append(readset)
        sample.add_readset(readset)

    log.info(str(len(readsets)) + " readset" + ("s" if len(readsets) > 1 else "") + " parsed")
    log.info(str(len(samples)) + " sample" + ("s" if len(samples) > 1 else "") + " parsed\n")
    return readsets



class DragenReadset(Readset):

    def __init__(self, name):
        super(DragenReadset, self).__init__(name)
        self.fastq1 = None
        self.fastq2 = None
        self.index1 = None
        self.index2 = None

    @property
    def fastq_folder(self):
        return self._fastq_folder

    @property
    def sample_number(self):
        return self._sample_number
    @property
    def fastqc_folder(self):
        return self._fastqc_folder


class IlluminaRawReadset(IlluminaReadset):

    def __init__(self, name, run_type):
        super(IlluminaRawReadset, self).__init__(name, run_type)

    @property
    def index(self):
        return self._index

    @property
    def fastq_folder(self):
        return self._fastq_folder

    @property
    def sample_number(self):
        return self._sample_number

    @property
    def aligner(self):
        return self._aligner

    @property
    def aligner_reference_index(self):
        return self._aligner_reference_index

    @property
    def description(self):
        return self._description

    @property
    def reference_file(self):
        return self._reference_file

    @property
    def is_rna(self):
        return self._is_rna

    @property
    def is_meth(self):
        return self._is_meth

    @property
    def annotation_files(self):
        if not hasattr(self, "_annotation_files"):
            return None
        else:
            return self._annotation_files

    @property
    def genomic_database(self):
        return self._genomic_database

    @property
    def project(self):
        return self._project

    @property
    def library_source(self):
        return self._library_source

    @property
    def library_type(self):
        return self._library_type

    @property
    def is_10x(self):
        return self._is_10x

    @property
    def is_10x_dual_index(self):
        return self._is_10x_dual_index

    @property
    def umi_index_number(self):
        return self._umi_index_number

    @property
    def umi_length(self):
        return self._umi_length

    @property
    def demultiplexing_method(self):
        return self._demultiplexing_method

    @property
    def result_file_type(self):
        return self._result_file_type

    @property
    def lane_str(self):
        return self._lane_str

    @property
    def are_lanes_merged(self):
        return self._are_lanes_merged

    @property
    def sequence_ready(self):
        return self._sequence_ready

    @property
    def has_exome(self):
        return self._has_exome

    @property
    def has_coverage(self):
        return self._has_coverage

    @property
    def fastqc_folder(self):
        return self._fastqc_folder

    @property
    def onboard_analysis(self):
        return self._onboard_analysis

    def cram_name(self):
        return self.name + ".cram"

    def crai_name(self):
        return self.name + ".cram.crai"

    def gvcf_name(self):
        return self.name + ".hard-filtered.gvcf.gz"

    def gvcf_tbi_name(self):
        return self.name + ".hard-filtered.gvcf.tbi"

    def sv_name(self):
        return self.name + ".sv.vcf.gz"

    def sv_tbi_name(self):
        return self.name + ".sv.vcf.gz.tbi"

    def cnv_name(self):
        return self.name + ".cnv.vcf.gz"

    def cnv_tbi_name(self):
        return self.name + ".cnv.vcf.gz.tbi"

    def quant_name(self):
        return self.name + ".quant.sf"

    def quant_genes_name(self):
        return self.name + ".quant.genes.sf"

    def fusion_vcf_name(self):
        return self.name + ".fusion_candidates.vcf.gz"

    def fusion_tbi_name(self):
        return self.name + ".fusion_candidates.vcf.gz.tbi"

    def fusion_candidate_features(self):
        return self.name + ".fusion_candidates.features.csv"

    def get_secondary_analysis_files(self):
        return [
            self.cram_name(),
            self.crai_name(),
            self.gvcf_name(),
            self.gvcf_tbi_name(),
            self.sv_name(),
            self.sv_tbi_name(),
            self.cnv_name(),
            self.cnv_tbi_name(),
            self.quant_name(),
            self.quant_genes_name(),
            self.fusion_vcf_name(),
            self.fusion_tbi_name(),
            self.fusion_candidate_features()
        ]

    @property
    def steps_eligibility_dict(self):
        return self._steps_eligibility_dict

    @property
    def is_metagenomic_library(self):
        return self._is_metagenomic_library

    @property
    def secondary_analysis_type(self):
        return self._secondary_analysis_type

    @property
    def secondary_analysis_zip(self):
        return self._secondary_analysis_zip

    def get_analysis_type_subfolder_structure(self):
        subfolder = "Data/" + self.secondary_analysis_type + "/" + self.name + "/"
        if self.secondary_analysis_type == "DragenGermline":
            return subfolder + "germline_seq"
        elif self.secondary_analysis_type == "DragenRna":
            return subfolder + "rna_seq"
        elif self.secondary_analysis_type == "DragenEnrichment":
            return subfolder + "enrichment_seq"

    def get_onboard_fastq_directory(self):
        analysis_folder = config.param("copy", "analysis_folder")
        return analysis_folder + self.secondary_analysis_type + "/fastq"

    def get_onboard_fastqc_directory(self):
        if self.secondary_analysis_type == "DragenGermline":
            subfolder = "germline_seq"
        elif self.secondary_analysis_type == "DragenRna":
            subfolder = "rna_seq"
        elif self.secondary_analysis_type == "DragenEnrichment":
            subfolder = "enrichment_seq"
        else:
            subfolder = "fastqc"
        analysis_folder = config.param("copy", "analysis_folder")
        return analysis_folder + self.secondary_analysis_type + "/" + self.name + "/" + subfolder


def get_onboard_fastq_directory(main_dir, readset):
    return os.path.join(
        main_dir,
        readset.get_onboard_fastq_directory()
    )

def get_onboard_fastq_file_name_pattern(main_dir, readset):
    return os.path.join(
        get_onboard_fastq_directory(main_dir, readset),
        readset.name + "_S" + readset.sample_number +
        "{lane}" + "_{read_number}_001.fastq.gz"
    )

def get_onboard_fastqc_directory(main_dir, readset):
    return os.path.join(
        main_dir,
        readset.get_onboard_fastqc_directory()
    )

def get_fastq_file_name_pattern(output_dir, readset, lane_str):
    return os.path.join(get_fastq_folder(output_dir, readset, lane_str),
                        readset.name + '_S' + readset.sample_number + '{lane}' + '_{read_number}_001.fastq.gz')


def get_fastq_folder(output_dir, readset, lane_str):
    return os.path.join(output_dir,
                        "Unaligned." + lane_str,
                        'Project_' + readset.project,
                        'Sample_' + readset.name)


def get_bcl_convert_fastq_file_name_pattern(output_dir, readset, lane_str, merge_lanes, read_number):
    filename = config.param("DEFAULT", "fastq_filename_format_string")
    return os.path.join(
        get_dragen_fastq_folder(output_dir, lane_str),
        filename.format(
            name=readset.name,
            sample_number=readset.sample_number,
            lane=("" if merge_lanes else ('_L00' + lane_str)),
            read_number=read_number
        )
    )


def get_dragen_fastq_folder(output_dir, lane_str):
    return os.path.join(output_dir, "Analysis", "1", "Data", "BCLConvert",
                        "fastq." + lane_str)


def get_dragen_fastqc_folder(output_dir, lane_str):
    return os.path.join(output_dir, "Analysis", "1", "Data", "BCLConvert",
                        "fastqc." + lane_str)


def parse_dragen_sample_sheet(output_dir, sheet_file):
    readsets = []
    # Parsing Sample Sheet
    log.info("Parsing Sample Sheet " + sheet_file + " ...")
    casava_csv = csv.DictReader(open(sheet_file, 'rb'), delimiter=',')
    readSampleName = False
    for line in casava_csv:
        if readSampleName:
            sample_name = line["[Header]"]
            readset = DragenReadset(sample_name)
            readset._sample_number = str(len(readsets) + 1)
            readset._fastq_folder = output_dir
            readset._fastqc_folder = output_dir
            fastq_file_pattern = os.path.join(output_dir, readset.name + "_S" + readset.sample_number + "_{read_number}_001.fastq.gz")
            readset.fastq1 = fastq_file_pattern.format(read_number="R1")
            readset.fastq2 = fastq_file_pattern.format(read_number="R2")

            readsets.append(readset)

            sample = Sample(sample_name)
            sample.add_readset(readset)

        if line["[Header]"] == "Sample_ID":
            readSampleName = True

    return readsets


def parse_illumina_raw_readset_file_for_secondary_analysis(run_type, nanuq_readset_file, dragen_sheet_file):
    readsets = []
    samples = []
    log.info("Parsing Sample Sheet " + dragen_sheet_file + " ...")

    date = ""
    if os.path.exists(dragen_sheet_file):
        epoch_time_sample_sheet = os.path.getctime(dragen_sheet_file)
        date = "_" + datetime.datetime.fromtimestamp(epoch_time_sample_sheet).strftime('%Y%m%d')

    dragen_file = open(dragen_sheet_file, 'rb')
    read_sample_name = False

    # Variable for different column position in dragen sample sheet
    name_pos = -1
    analysis_type_capture_regex = re.compile("^\\[(.*)_.*\\]$")
    analysis_type = ""
    for line in dragen_file:
        split_line = line.split(",")
        analysis_type_regex_result = analysis_type_capture_regex.search(split_line[0])
        if read_sample_name and analysis_type_regex_result:
            # New section
            analysis_type = ""
            read_sample_name = False
            name_pos = -1

        if read_sample_name:
            # Probably an empty line
            if len(split_line) <= 1:
                continue
            # if lane_pos is not equal to -1, we have a lane column in the file.
            # We compared that number to the lane passed by argument. If they differ, we skip
            # Run merged will have no lane column and will process all the readsets.

            sample_name = split_line[name_pos]
            readset = IlluminaRawReadset(sample_name, run_type)
            readset._sample_number = str(len(readsets) + 1)
            readset._secondary_analysis_type = analysis_type
            readsets.append(readset)
        # Sample section should start with
        if analysis_type_regex_result and split_line[0].rstrip() != "[BCLConvert_Data]" and "_Data]" in split_line[0]:
            analysis_type = analysis_type_regex_result.group(1)
            split_line = dragen_file.next().split(",")
            read_sample_name = True
            for i in range(0, len(split_line)):
                if split_line[i] == "Sample_ID":
                    name_pos = i

    # Parsing Nanuq readset sheet
    log.info("Parse Nanuq Illumina readset file " + nanuq_readset_file + " ...")
    readset_csv = csv.DictReader(open(nanuq_readset_file, 'rb'), delimiter=',', quotechar='"')

    for line in readset_csv:
        processing_sheet_id = line['ProcessingSheetId']
        readset = [x for x in readsets if x.name == processing_sheet_id]
        if readset:
            readset = readset[0]
        else:
            log.info("Skipping library: " + line["Library Name"])
            continue
        # Always create a new sample
        sample = Sample(line['Name'])
        samples.append(sample)
        sample.add_readset(readset)
        readset._lane_str = line["Region"]
        readset._run = line["Run"]
        readset._secondary_analysis_zip = readset.run + "_" + readset.secondary_analysis_type + "_" + readset.name + date + ".zip"

    return readsets


def extract_readset_dragen_file(dragen_sheet_file, lane, lane_str, run_type, steps):
    readsets = []
    merge_lanes = False
    if lane is None:
        merge_lanes = True
        lane = 1

    enable_all = [1 for i in xrange(len(steps))]

    # Parsing Sample Sheet
    log.info("Extracting readset from Sample Sheet " + dragen_sheet_file + " ...")
    dragen_file = open(dragen_sheet_file, 'rb')
    read_sample_name = False

    # Variable for different column position in dragen sample sheet
    name_pos = -1
    lane_pos = -1
    index_pos = -1
    index2_pos = -1
    new_section_regex = re.compile("^\[.*\]$")

    for line in dragen_file:
        split_line = line.split(",")
        if read_sample_name and new_section_regex.search(split_line[0]):
            # Done with Data section, skip the rest
            break
        if read_sample_name:
            # Probably an empty line
            if len(split_line) <= 1:
                continue
            # if lane_pos is not equal to -1, we have a lane column in the file.
            # We compared that number to the lane passed by argument. If they differ, we skip
            # Run merged will have no lane column and will process all the readsets.

            if lane_pos != -1 and int(split_line[lane_pos]) != lane:
                continue

            sample_name = split_line[name_pos]
            readset = IlluminaRawReadset(sample_name, run_type)
            readset._secondary_analysis_type = "BCLConvert"

            # Index1-Index2
            index_string = split_line[index_pos]
            if index2_pos != -1:
                index_string += "-" + split_line[index2_pos]
            readset._index = index_string

            readset._sample_number = str(len(readsets) + 1)
            readset._lane_str = lane_str
            readset._are_lanes_merged = merge_lanes
            readset._steps_eligibility_dict = dict(zip(steps, enable_all))
            readsets.append(readset)

        # Sample section should start with
        if split_line[0] == "[BCLConvert_Data]":
            split_line = dragen_file.next().split(",")
            read_sample_name = True
            for i in range(0, len(split_line)):
                if split_line[i] == "Sample_ID":
                    name_pos = i
                elif split_line[i] == "Index":
                    index_pos = i
                elif split_line[i] == "Index2":
                    index2_pos = i
                elif split_line[i] == "Lane":
                    lane_pos = i
    dragen_file.close()
    return readsets

def populate_secondary_analysis_type(readsets, dragen_sheet_file):
    # Reading other secondary analysis sections
    log.info("Extracting secondary analysis readsets")
    name_pos = -1
    read_sample_name = False
    analysis_type_capture_regex = re.compile("^\\[(.*)_.*\\]$")
    analysis_type = ""
    dragen_file = open(dragen_sheet_file, 'rb')

    for line in dragen_file:
        split_line = line.split(",")
        analysis_type_regex_result = analysis_type_capture_regex.search(split_line[0])
        if read_sample_name and analysis_type_regex_result:
            # New section
            analysis_type = ""
            read_sample_name = False
            name_pos = -1

        if read_sample_name:
            # Probably an empty line
            if len(split_line) <= 1:
                continue
            # if lane_pos is not equal to -1, we have a lane column in the file.
            # We compared that number to the lane passed by argument. If they differ, we skip
            # Run merged will have no lane column and will process all the readsets.

            sample_name = split_line[name_pos]
            readset = [x for x in readsets if x.name == sample_name]
            if readset:
                readset = readset[0]
                readset._secondary_analysis_type = analysis_type

        # Sample section should start with
        if analysis_type_regex_result and split_line[0].rstrip() != "[BCLConvert_Data]" and "_Data]" in split_line[0]:
            analysis_type = analysis_type_regex_result.group(1)
            split_line = dragen_file.next().split(",")
            read_sample_name = True
            for i in range(0, len(split_line)):
                if split_line[i] == "Sample_ID":
                    name_pos = i

    dragen_file.close()

def populate_readset_to_onboard_fastq_folder(readsets, run_dir, lane_str, merge_lanes):
    for readset in readsets:
        onboard_fastq_file_pattern = get_onboard_fastq_file_name_pattern(run_dir, readset)
        readset.fastq1 = onboard_fastq_file_pattern.format(read_number="R1", lane=("" if merge_lanes else ('_L00' + lane_str)))
        if readset.run_type == "PAIRED_END":
            readset.fastq2 = onboard_fastq_file_pattern.format(read_number="R2", lane=("" if merge_lanes else ('_L00' + lane_str)))
        else:
            readset.fastq2 = None
        readset._fastq_folder = get_onboard_fastq_directory(run_dir, readset)
        readset._fastqc_folder = get_dragen_fastqc_folder(run_dir, lane_str)
        if not os.path.isfile(readset.fastq1):
            log.warn("Cannot find file Fastq1: " + readset.fastq1)


def populate_readset_to_fastq_folder(readsets, output_dir, lane_str, merge_lanes):
    for readset in readsets:
        readset._fastq_folder = get_dragen_fastq_folder(output_dir, lane_str)
        readset._fastqc_folder = get_dragen_fastqc_folder(output_dir, lane_str)

        readset.fastq1 = get_bcl_convert_fastq_file_name_pattern(output_dir, readset, lane_str, merge_lanes, "R1")
        if readset.run_type == "PAIRED_END":
            readset.fastq2 = get_bcl_convert_fastq_file_name_pattern(output_dir, readset, lane_str, merge_lanes, "R2")
        else:
            readset.fastq2 = None

def determine_readset_steps_eligibility(readsets):
    for readset in readsets:
        if readset._onboard_analysis:
            readset._steps_eligibility_dict["fastq"] = 0
            readset._steps_eligibility_dict["align"] = 0
            readset._steps_eligibility_dict["picard_mark_duplicates"] = 0
            readset._steps_eligibility_dict["metrics"] = 0
        else:
            # If we determine steps eligibility, we use onboard fastq.
            readset._steps_eligibility_dict["fastq"] = 0

def parse_illumina_raw_readset_files(run_dir, output_dir, run_type, nanuq_readset_file, dragen_sheet_file, lane, lane_str, genome_root,
                                     skip_alignment_library_type_regex, skip_alignment_for_fastq, nb_index, steps, readset_onboard_filtering):
    samples = []
    GenomeBuild = namedtuple('GenomeBuild', 'species assembly')

    merge_lanes = False
    if lane is None:
        merge_lanes = True
        lane = 1

    readsets = extract_readset_dragen_file(dragen_sheet_file, lane, lane_str, run_type, steps)
    populate_secondary_analysis_type(readsets, dragen_sheet_file)

    # Parsing Nanuq readset sheet
    log.info("Parse Nanuq Illumina readset file " + nanuq_readset_file + " ...")
    readset_csv = csv.DictReader(open(nanuq_readset_file, 'rb'), delimiter=',', quotechar='"')

    sample_number = 1
    unique_sample_name = dict()

    for line in readset_csv:
        current_lane = line['Processing Region']

        processing_sheet_id = line['ProcessingSheetId']
        if not processing_sheet_id in unique_sample_name:
            unique_sample_name[processing_sheet_id] = sample_number
            sample_number += 1


        if int(current_lane) != lane:
            continue

        readsets_by_name = [x for x in readsets if x.name == processing_sheet_id]

        if not readsets_by_name:
            log.info("Skipping library: " + line["Library Name"])
            continue

        for readset in readsets_by_name:
            # Always create a new sample
            sample = Sample(line['Name'])
            samples.append(sample)
            sample.add_readset(readset)

            # The sample ordering of onboard fastq is different since the whole run is demultiplexed and not only the lane
            # sample_number is already defined when we extract the readsets from the dragen sample sheet.
            if readset_onboard_filtering:
                readset._sample_number = str(unique_sample_name[processing_sheet_id])

            readset._quality_offset = 33
            readset._library = line['Library Barcode']
            readset._library_source = line['Library Source']
            readset._library_type = line['Library Type']
            readset._demultiplexing_method = line['Demultiplexing Method']
            readset._genomic_database = line['Genomic Database']
            readset._description = line["Multiplex Key(s)"]
            umi_read_number_raw_value = line['UMI Index Read Number']
            readset._umi_index_number = int(umi_read_number_raw_value) if len(umi_read_number_raw_value) > 0 else 0
            readset._umi_length = int(line['UMI Length']) if (len(line['UMI Length']) > 0) else 0
            readset._sequence_ready = True if line['Sequence-Ready'] == "true" else False
            readset._has_exome = True if line['Library Type has Exome'] == "true" else False
            readset._has_coverage = True if line['Sequencing Type has Coverage'] == "true" else False
            readset._is_meth = (readset.library_source in ("WGBS","RRBS")) or (readset.library_type == "Methyl-Seq")

            if 'Onboard Analysis' in line:
                readset._onboard_analysis = True if line['Onboard Analysis'] == "true" else False
            else:
                readset._onboard_analysis = False

            if not readset.is_meth and 'Result File Type' in line and line['Result File Type']:
                readset._result_file_type = line['Result File Type']
            else:
                readset._result_file_type = "FASTQ"

            readset._run = line['Run']

            readset._is_rna = readset.library_type != "Covid-Seq" and \
                                (re.search("RNA|cDNA", readset.library_source) or (readset.library_source == "Library" and re.search("RNA", readset.library_type)))
            readset._is_10x = readset.demultiplexing_method == "chromium_rna"

            if line['BED Files']:
                readset._beds = line['BED Files'].split(";")
            else:
                readset._beds = []

            if 'Forward Primer' in line or 'Reverse Primer' in line:
                readset._is_metagenomic_library = line["Forward Primer"] or line["Reverse Primer"]
            else:
                readset._is_metagenomic_library = False


    # Searching for a matching reference for the specified species
    for readset in readsets:
        genome_build = None
        if readset.is_meth:
            readset._aligner = BismarkRunProcessingAligner(output_dir, config.param('methylathion_ref', 'genome_dir'))
        else:
            readset._aligner = NullRunProcessingAligner(output_dir, None)
            m = re.search("(?P<build>\w+):(?P<assembly>[^:]+)", readset.genomic_database)

            if m:
                genome_build = GenomeBuild(m.group('build'), m.group('assembly'))

            if genome_build is not None:
                folder_name = os.path.join(genome_build.species + "." + genome_build.assembly)
                current_genome_folder = genome_root + os.sep + folder_name

                if (skip_alignment_library_type_regex and re.match(skip_alignment_library_type_regex, readset.library_type)):
                    readset._aligner = NullRunProcessingAligner(output_dir, current_genome_folder)
                elif readset.result_file_type == "FASTQ" and skip_alignment_for_fastq == "1":
                    readset._aligner = NullRunProcessingAligner(output_dir, current_genome_folder)
                elif readset.is_10x:
                    readset._aligner = CellrangerRunProcessingAligner(output_dir, current_genome_folder)
                elif readset.sequence_ready:
                    readset._aligner = NullRunProcessingAligner(output_dir, current_genome_folder)
                elif readset.is_rna:
                    readset._aligner = NullRunProcessingAligner(output_dir, current_genome_folder)
                elif readset.has_exome or readset.has_coverage:
                    readset._aligner = BwaRunProcessingAligner(output_dir, current_genome_folder)
                else:
                    readset._aligner = NullRunProcessingAligner(output_dir, current_genome_folder)


        if genome_build is not None or readset.is_meth:
            aligner_reference_index = readset.aligner.get_reference_index()
            annotation_files = readset.aligner.get_annotation_files()
            reference_file = readset.aligner.get_reference_file()
            if reference_file and os.path.isfile(reference_file) and aligner_reference_index:
                if os.path.isfile(aligner_reference_index) or os.path.isdir(aligner_reference_index):
                    readset._aligner_reference_index = aligner_reference_index
                    readset._annotation_files = annotation_files
                    readset._reference_file = reference_file
                    readset._bam = os.path.join(output_dir,
                                                "Aligned." + readset.lane_str,
                                                'alignment',
                                                readset.sample.name,
                                                'run' + readset.run + "_" + readset.lane_str,
                                                readset.sample.name + "." + readset.library + ".sorted")
                else:
                    log.warning("Unable to access the aligner reference file: '" + aligner_reference_index +
                                "' for aligner: '" + readset.aligner.__class__.__name__ + "'")
            elif reference_file:
                log.warning("Unable to access the reference file: '" + reference_file + "'")
        if readset.bam is None and len(readset.genomic_database) > 0:
            log.info("Skipping alignment for the genomic database: '" + readset.genomic_database +
                     "' and library type: '" + readset.library_type + "'" )

    if readset_onboard_filtering:
        populate_readset_to_onboard_fastq_folder(readsets, run_dir, lane_str, merge_lanes)
        determine_readset_steps_eligibility(readsets)
    else:
        populate_readset_to_fastq_folder(readsets, output_dir, lane_str, merge_lanes)

    log.info(str(len(readsets)) + " readset" + ("s" if len(readsets) > 1 else "") + " parsed")
    log.info(str(len(samples)) + " sample" + ("s" if len(samples) > 1 else "") + " parsed\n")
    for i in range(1, nb_index+1):
        for readset in readsets:
            setattr(readset, "index" + str(i), re.sub("(?<!Sample)_R1_", "_I" + str(i) + "_", readset.fastq1))

    return readsets

class SequelRawReadset(Readset):

    def __init__(self, name):
        super(SequelRawReadset, self).__init__(name)

    @property
    def subreads_file(self):
        return self._subreads_file

    @property
    def ccs_file(self):
        return self._ccs_file

    @property
    def whitelist_file(self):
        return self._whitelist_file

    @property
    def is_main(self):
        return self._is_main

    @property
    def is_unlabeled(self):
        return self._is_unlabeled

    @property
    def is_symmetric(self):
        return self._is_symmetric

def parse_sequel_raw_readset_files(dictionary_file, output_dir, movie_name):
    readsets = []

    # main readset, always there barcoded or not
    readset = SequelRawReadset("main")
    readset._subreads_file = output_dir + os.path.sep + movie_name + ".subreads.bam"
    readset._ccs_file = output_dir + os.path.sep + movie_name + ".ccs.bam"
    readset._is_main = True
    readset._is_unlabeled = False
    readset._is_symmetric = False
    readsets.append(readset)

    # Load dictionary file, for barcoded cells
    with open(dictionary_file) as f:
        for line in f:
            # moviename.bcAd1022T_Forward|0,0
            tokens = line.strip().split("|")
            file_prefix = tokens[0]
            idx = tokens[1].split(",")
            is_symmetric = (idx[0] == idx[1])  # symmetric when the same barcode index is used
            readset = SequelRawReadset(file_prefix)
            readset._subreads_file = output_dir + os.path.sep + file_prefix + ".subreads.bam"
            readset._ccs_file = output_dir + os.path.sep + file_prefix + ".ccs.bam"
            readset._whitelist_file =  output_dir + os.path.sep + file_prefix + ".whitelist"
            readset._is_main = False
            readset._is_unlabeled = False
            readset._is_symmetric = is_symmetric
            readsets.append(readset)

    # add unlabeled when barcoded
    if len(readsets) > 1:
        readset = SequelRawReadset("UNLABELED")
        readset._subreads_file = output_dir + os.path.sep + movie_name + ".UNLABELED.subreads.bam"
        readset._ccs_file = output_dir + os.path.sep + movie_name + ".UNLABELED.ccs.bam"
        readset._whitelist_file =  output_dir + os.path.sep + movie_name + ".UNLABELED.whitelist"
        readset._is_main = False
        readset._is_unlabeled = True
        readset._is_symmetric = False
        readsets.append(readset)

    return readsets


class PacBioReadset(Readset):

    @property
    def run(self):
        return self._run

    @property
    def smartcell(self):
        return self._smartcell

    @property
    def protocol(self):
        return self._protocol

    @property
    def nb_base_pairs(self):
        return self._nb_base_pairs

    @property
    def estimated_genome_size(self):
        if self._estimated_genome_size:
            return self._estimated_genome_size
        else:
            raise Exception("Error: readset \"" + self.name + "\" estimated_genome_size is not defined!")

    @property
    def bas_files(self):
        return self._bas_files

    @property
    def bax_files(self):
        return self._bax_files

    @property
    def bam_file(self):
        return self._bam_file

def parse_pacbio_readset_file(pacbio_readset_file):
    readsets = []
    samples = []

    log.info("Parse PacBio readset file " + pacbio_readset_file + " ...")
    readset_csv = csv.DictReader(open(pacbio_readset_file, 'rb'), delimiter='\t')
    for line in readset_csv:
        sample_name = line['Sample']
        sample_names = [sample.name for sample in samples]
        if sample_name in sample_names:
            # Sample already exists
            sample = samples[sample_names.index(sample_name)]
        else:
            # Create new sample
            sample = Sample(sample_name)
            samples.append(sample)

        # Create readset and add it to sample
        readset = PacBioReadset(line['Readset'])

        # Readset file paths are either absolute or relative to the readset file
        # Convert them to absolute paths
        for format in ("BAS", "BAX"):
            if line.get(format, None):
                abs_files = []
                for file in line[format].split(","):
                    file = os.path.expandvars(file)
                    if not os.path.isabs(file):
                        file = os.path.dirname(os.path.abspath(os.path.expandvars(pacbio_readset_file))) + os.sep + file
                    abs_files.append(os.path.normpath(file))
                line[format] = ",".join(abs_files)

        readset._run = line.get('Run', None)
        readset._smartcell = line.get('Smartcell', None)
        readset._protocol = line.get('Protocol', None)
        readset._nb_base_pairs = int(line['NbBasePairs']) if line.get('NbBasePairs', None) else None
        readset._estimated_genome_size = int(line['EstimatedGenomeSize']) if line.get('EstimatedGenomeSize', None) else None
        readset._bas_files = line['BAS'].split(",") if line.get('BAS', None) else []
        readset._bax_files = line['BAX'].split(",") if line.get('BAX', None) else []
        readset._bam_file = line['BAM'] if line.get('BAM', None) else None

        readsets.append(readset)
        sample.add_readset(readset)

    log.info(str(len(readsets)) + " readset" + ("s" if len(readsets) > 1 else "") + " parsed")
    log.info(str(len(samples)) + " sample" + ("s" if len(samples) > 1 else "") + " parsed\n")
    return readsets
