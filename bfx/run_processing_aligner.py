#!/usr/bin/env python

################################################################################
# Copyright (C) 2014, 2015 GenAP, McGill University and Genome Quebec Innovation Centre
#
# This file is part of MUGQIC Pipelines.
#
# MUGQIC Pipelines is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MUGQIC Pipelines is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MUGQIC Pipelines.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
import re

from core.job import *
from core.config import *
from bfx import bvatools
from bfx import verify_bam_id
from bfx import bwa
from bfx import picard
from bfx import tools
from bfx import cellranger
from bfx import bismark
from bfx import trimmomatic

import logging

log = logging.getLogger(__name__)


class RunProcessingAligner(object):
    def __init__(self, output_dir, genome_folder):
        self._output_dir = output_dir
        self._genome_folder = genome_folder

    @property
    def output_dir(self):
        return self._output_dir

    @property
    def genome_folder(self):
        return self._genome_folder

    def get_reference_index(self):
        raise NotImplementedError("Please Implement this method")

    def get_reference_file(self):
        raise NotImplementedError("Please Implement this method")

    def get_alignment_job(self, readset):
        raise NotImplementedError("Please Implement this method")

    def get_metrics_jobs(self, readset):
        raise NotImplementedError("Please Implement this method")

    def get_fastq_metrics_jobs(self, readset):
        return []

    def get_annotation_files(self):
        raise NotImplementedError("Please Implement this method")

    def is_run_mark_duplicate(self):
        return True

    @staticmethod
    def get_rg_tag(readset, ini_section):
        tag = "'@RG" + \
            "\tID:" + readset.library + "_" + readset.run + ("" if readset.are_lanes_merged else ("_" + readset.lane_str)) + \
            "\tSM:" + readset.sample.name + \
            "\tLB:" + readset.library + \
            "\tPU:run" + readset.run + ("" if readset.are_lanes_merged else ("_" + readset.lane_str)) + \
            ("\tCN:" + config.param(ini_section, 'sequencing_center')
            if config.param(ini_section, 'sequencing_center', required=False) else "") + \
            "\tPL:Illumina" + \
            "'"
        tag = re.sub('[ \r\f\v]', '_', tag)
        return tag

    @staticmethod
    def find_10x_synonym_reference(parent_genome_folder, synonym, option):
        trans_path = ""
        assembly_name = os.path.basename(parent_genome_folder)
        species = assembly_name.split(".")[0]
        main_genome_folder = os.path.dirname(parent_genome_folder)
        new_assembly = species + "." + synonym
        synonym_path = os.path.join(main_genome_folder, new_assembly)
        ini_file = os.path.join(synonym_path + os.sep + new_assembly + ".ini")

        if os.path.isfile(ini_file):
            genome_config = ConfigParser.SafeConfigParser()
            genome_config.read(ini_file)

            if genome_config.has_option("DEFAULT", option):
                transcriptome = genome_config.get("DEFAULT", option)
                if transcriptome:
                    trans_path = os.path.join(synonym_path, "genome", "10xGenomics", transcriptome)
        return trans_path


class NullRunProcessingAligner(RunProcessingAligner):
    """ A no-op aligner used when we want to skip the alignment """

    def get_reference_index(self):
        return None

    def get_reference_file(self):
        return None

    def get_alignment_job(self, readset):
        return []

    def get_metrics_jobs(self, readset):
        return []

    def get_annotation_files(self):
        return []


class BwaRunProcessingAligner(RunProcessingAligner):
    downloaded_bed_files = []
    created_interval_lists = []

    def get_reference_index(self):
        folder_name = os.path.basename(self.genome_folder)
        return os.path.join(self.genome_folder,
                            "genome",
                            "bwa_index",
                            folder_name + ".fa")

    def get_reference_file(self):
        folder_name = os.path.basename(self.genome_folder)
        return os.path.join(self.genome_folder,
                            "genome",
                            folder_name + ".fa")

    def get_annotation_files(self):
        folder_name = os.path.basename(self.genome_folder)
        ini_file = os.path.join(self.genome_folder + os.sep + folder_name + ".ini")
        found_annotations_file = []
        if os.path.isfile(ini_file):
            genome_config = ConfigParser.SafeConfigParser()
            genome_config.read(ini_file)

            section = "DEFAULT"
            dbsnp_option_name = "dbsnp_version"
            af_option_name = "population_AF"

            if genome_config.has_option(section, dbsnp_option_name) and \
                    genome_config.has_option(section, af_option_name):
                dbsnp_version = genome_config.get(section, dbsnp_option_name)
                af_name = genome_config.get(section, af_option_name)
                file_to_find = folder_name + ".dbSNP" + dbsnp_version + "_" + af_name + ".vcf"

                if os.path.isfile(os.path.join(self.genome_folder, "annotations", file_to_find)):
                    found_annotations_file.append(os.path.join(self.genome_folder, "annotations", file_to_find))

                # If the .vcf is not found, maybe it is gzipped
                if len(found_annotations_file) == 0:
                    if os.path.isfile(os.path.join(self.genome_folder, "annotations", file_to_find + ".gz")):
                        found_annotations_file.append(os.path.join(self.genome_folder, "annotations", file_to_find + ".gz"))

        return found_annotations_file

    def get_alignment_job(self, readset):
        output = readset.bam + ".bam"
        job = concat_jobs([
            Job(command="mkdir -p " + os.path.dirname(output)),
            pipe_jobs([
                bwa.mem(
                    readset.fastq1,
                    readset.fastq2,
                    read_group=RunProcessingAligner.get_rg_tag(readset, 'bwa_mem'),
                    ref=readset.aligner_reference_index
                ),
                picard.sort_sam(
                    "/dev/stdin",
                    output,
                    "coordinate"
                )
            ])
        ], name="bwa_mem_picard_sort_sam." + readset.name + "." + readset.run + "." + readset.lane_str)

        job.samples = [readset]
        return [job]

    def get_metrics_jobs(self, readset):
        jobs = []

        input_file_prefix = readset.bam + '.'
        input = input_file_prefix + "bam"

        job = picard.collect_multiple_metrics(input, input_file_prefix + "metrics",
                                              reference_sequence=readset.reference_file)
        job.name = "picard_collect_multiple_metrics." + readset.name + ".met" + "." + readset.run + "." + readset.lane_str
        jobs.append(job)

        if readset.beds:
            coverage_bed = readset.beds[0]
            full_coverage_bed = (self.output_dir + os.sep + coverage_bed)
        else:
            coverage_bed = None
            full_coverage_bed = None

        if coverage_bed:
            if (not os.path.exists(full_coverage_bed)) and \
                    (coverage_bed not in BwaRunProcessingAligner.downloaded_bed_files):
                # Download the bed file
                command = config.param('DEFAULT', 'fetch_bed_file_command').format(
                    output_directory=self.output_dir,
                    filename=coverage_bed
                )
                job = Job([], [full_coverage_bed], command=command, name="bed_download." + coverage_bed)
                BwaRunProcessingAligner.downloaded_bed_files.append(coverage_bed)
                jobs.append(job)

            interval_list = re.sub("\.[^.]+$", ".interval_list", coverage_bed)

            if interval_list not in BwaRunProcessingAligner.created_interval_lists:
                # Create one job to generate the interval list from the bed file
                ref_dict = os.path.splitext(readset.reference_file)[0] + '.dict'
                job = tools.bed2interval_list(ref_dict, full_coverage_bed, interval_list)
                job.name = "interval_list." + coverage_bed
                BwaRunProcessingAligner.created_interval_lists.append(interval_list)
                jobs.append(job)

            job = picard.calculate_hs_metrics(input_file_prefix + "bam", input_file_prefix + "metrics.onTarget.txt",
                                              interval_list, reference_sequence=readset.reference_file)
            job.name = "picard_calculate_hs_metrics." + readset.name + ".hs" + "." + readset.run + "." + readset.lane_str
            jobs.append(job)

        jobs.extend(self.verify_bam_id(readset))

        job = bvatools.depth_of_coverage(
            input,
            input_file_prefix + "metrics.targetCoverage.txt",
            full_coverage_bed,
            other_options=config.param('bvatools_depth_of_coverage', 'other_options', required=False),
            reference_genome=readset.reference_file
        )
        job.name = "bvatools_depth_of_coverage." + readset.name + ".doc" + "." + readset.run + "." + readset.lane_str
        jobs.append(job)

        return jobs

    def verify_bam_id(self, readset):
        """
            verifyBamID is a software that verifies whether the reads in particular file match previously known
            genotypes for an individual (or group of individuals), and checks whether the reads are contaminated
            as a mixture of two samples. verifyBamID can detect sample contamination and swaps when external
            genotypes are available. When external genotypes are not available, verifyBamID still robustly
            detects sample swaps.
        """
        jobs = []
        if len(readset.annotation_files) > 0 and os.path.isfile(readset.annotation_files[0]):
            known_variants_annotated = readset.annotation_files[0]
            known_variants_annotated_filtered = known_variants_annotated

            input_bam = readset.bam + ".bam"
            output_prefix = readset.bam + ".metrics.verifyBamId"

            jobs.append(concat_jobs([
                verify_bam_id.verify(
                    input_bam,
                    known_variants_annotated_filtered,
                    output_prefix,
                ),
                # the first column starts with a # (a comment for nanuq) so we remove the column and output the result
                # in a file with the name supported by nanuq
                Job([output_prefix + ".selfSM"],
                    [output_prefix + ".tsv"],
                    command="cut -f2- " + output_prefix + ".selfSM > " + output_prefix + ".tsv"
                    )
            ],
                name="verify_bam_id." + readset.name + "." + readset.run + "." + readset.lane_str
            ))

        return jobs


class CellrangerRunProcessingAligner(RunProcessingAligner):
    def get_annotation_files(self):
        return []

    def get_reference_index(self):
        assembly_name = os.path.basename(self.genome_folder)
        ini_file = os.path.join(self.genome_folder + os.sep + assembly_name + ".ini")
        if os.path.isfile(ini_file):
            genome_config = ConfigParser.SafeConfigParser()
            genome_config.read(ini_file)
            trans_path = ""

            if genome_config.has_option("DEFAULT", "10x_transcriptome"):
                transcriptome = genome_config.get("DEFAULT", "10x_transcriptome")
                trans_path = os.path.join(self.genome_folder, "genome", "10xGenomics", transcriptome)

            if trans_path is None or not os.path.isdir(trans_path):
                """ Main transcriptome not found, we will try to resolve it if synomym exists """
                if genome_config.has_option("DEFAULT", "assembly_synonyms"):
                    synonym = genome_config.get("DEFAULT", "assembly_synonyms")
                    trans_path = self.find_10x_synonym_reference(self.genome_folder, synonym, "10x_transcriptome")
            return trans_path
        else:
            return ""

    def get_reference_file(self):
        return os.path.join(self.get_reference_index(), "fasta", "genome.fa")

    def get_alignment_job(self, readset):

        fastqs=os.path.dirname(readset.fastq1)

        cell_count_job = cellranger.count(
            reads1=readset.fastq1,
            reads2=readset.fastq2,
            sample_id=readset.name + "_" + readset.sample_number,
            sample=readset.name,
            lane_id=readset.lane_str,
            fastqs=fastqs,
            transcriptome=self.get_reference_index()
        )
        default_work_dir = config.param('cellranger_count', 'working_dir', required=True)
        cell_work_dir = os.path.join(self.output_dir, default_work_dir + "_" + readset.lane_str, readset.name + "_" + readset.sample_number)
        cell_count_copy = cellranger.move_count_files(cell_work_dir, readset)
        cell_count_job.output_files = cell_count_copy.input_files
        job = concat_jobs([Job(command="mkdir -p " + os.path.dirname(readset.bam + ".bam")), cell_count_job, cell_count_copy])
        job.name = "cellranger_count." + readset.name + "." + readset.run + "." + readset.lane_str
        job.samples = [readset]
        return [job]

    def get_metrics_jobs(self, readset):
        return []


class BismarkRunProcessingAligner(RunProcessingAligner):
    downloaded_bed_files = []
    created_interval_lists = []

    def get_alignment_job_name(self, readset):
        return "bismark_align_spikeIn." + readset.name + "." + readset.run + "." + readset.lane_str

    def get_metrics_cpg_stats_job_name(self, readset):
        return "methylcall_CpG_stats_spikeIn." + readset.name + "." + readset.run + "." + readset.lane_str

    def get_temporary_files_sample_name_to_delete(self):
        return [
            "CpG_context_*.dup.sortedN.txt.gz",
            "CHH_context_*.dup.sortedN.txt.gz",
            "CHG_context_*.dup.sortedN.txt.gz",
            "*.dup.sortedN.bismark.cov.gz",
            "*.dup.sortedN.bedGraph.gz",
            "*.dup.sortedN.CpG_report.txt.gz"
        ]

    def get_temporary_files_readset_name_to_delete(self):
        return [
            "*.trimmomatic.log",
            "*.trimmed.U2.fastq.gz",
            "*.trimmed.U1.fastq.gz",
            "*.trimmed.R2.fastq.gz",
            "*.trimmed.R1.fastq.gz",
            "*.bismark.tmp"
        ]


    def get_reference_index(self):
        folder_name = os.path.basename(self.genome_folder)
        return os.path.join(self.genome_folder,
                            "genome/bismark_index/")

    def get_reference_file(self):
        folder_name = os.path.basename(self.genome_folder)
        return os.path.join(self.genome_folder,
                            "genome/bismark_index/",
                            folder_name + ".fa")

    def get_annotation_files(self):
        folder_name = os.path.basename(self.genome_folder)
        ini_file = os.path.join(self.genome_folder + os.sep + folder_name + ".ini")
        found_annotations_file = []
        if os.path.isfile(ini_file):
            genome_config = ConfigParser.SafeConfigParser()
            genome_config.read(ini_file)

            section = "DEFAULT"
            dbsnp_option_name = "dbsnp_version"
            af_option_name = "population_AF"

            if genome_config.has_option(section, dbsnp_option_name) and \
                    genome_config.has_option(section, af_option_name):
                dbsnp_version = genome_config.get(section, dbsnp_option_name)
                af_name = genome_config.get(section, af_option_name)
                file_to_find = folder_name + ".dbSNP" + dbsnp_version + "_" + af_name + ".vcf"

                if os.path.isfile(os.path.join(self.genome_folder, "annotations", file_to_find)):
                    found_annotations_file.append(os.path.join(self.genome_folder, "annotations", file_to_find))

                # If the .vcf is not found, maybe it is gzipped
                if len(found_annotations_file) == 0:
                    if os.path.isfile(os.path.join(self.genome_folder, "annotations", file_to_find + ".gz")):
                        found_annotations_file.append(os.path.join(self.genome_folder, "annotations", file_to_find + ".gz"))

        return found_annotations_file

    def get_alignment_job(self, readset):
        output = readset.bam + ".bam"
        output_path = os.path.join("Aligned." + readset.lane_str,'alignment',readset.sample.name,'run' + readset.run + "_" + readset.lane_str)
        unsorted_output = os.path.join(self.output_dir, output_path,readset.name + '_S' + readset.sample_number + readset.lane_str + '.trimmed.R1_bismark_bt2_pe.bam')

        jobs = []

        make_output_directory_job = Job(command="mkdir -p " + os.path.dirname(output))
        jobs.append(
            concat_jobs([
                make_output_directory_job,
                trimmomatic.trimmomatic(
                    input1=readset.fastq1,
                    input2=readset.fastq2,
                    paired_output1=os.path.join(self.output_dir,output_path,readset.name + '_S' + readset.sample_number + readset.lane_str + '.trimmed.R1.fastq.gz'),
                    unpaired_output1=os.path.join(self.output_dir,output_path,readset.name + '_S' + readset.sample_number + readset.lane_str + '.trimmed.U1.fastq.gz'),
                    paired_output2=os.path.join(self.output_dir,output_path,readset.name + '_S' + readset.sample_number + readset.lane_str + '.trimmed.R2.fastq.gz'),
                    unpaired_output2=os.path.join(self.output_dir,output_path,readset.name + '_S' + readset.sample_number + readset.lane_str + '.trimmed.U2.fastq.gz'),
                    single_output="",
                    quality_offset=readset.quality_offset,
                    adapter_file="/cvmfs/soft.mugqic/CentOS6/software/trimmomatic/Trimmomatic-0.39/adapters/TruSeq3-PE-2.fa",
                    trim_log=os.path.join(self.output_dir,output_path,readset.name + '_S' + readset.sample_number + readset.lane_str + '.trimmomatic.log')
                )
            ], name="trimmomatic." + readset.name + "." + readset.run + "." + readset.lane_str, samples=[readset])
        )

        jobs.append(
            concat_jobs([
                make_output_directory_job,
                bismark.align_gq(
                    input1=os.path.join(self.output_dir,output_path,readset.name + '_S' + readset.sample_number + readset.lane_str + '.trimmed.R1.fastq.gz'),
                    input2=os.path.join(self.output_dir,output_path,readset.name + '_S' + readset.sample_number + readset.lane_str + '.trimmed.R2.fastq.gz'),
                    genome_directory=readset.aligner_reference_index,
                    rg_id=readset.library + "_" + readset.run + ("" if readset.are_lanes_merged else ("_" + readset.lane_str)),
                    rg_sample=readset.sample.name,
                    output=unsorted_output,
                    output_dir=os.path.join(self.output_dir,output_path),
                    tmp_dir=os.path.join(self.output_dir,output_path,readset.name + '_S' + readset.sample_number + readset.lane_str + '.bismark.tmp')
                )
            ], name=self.get_alignment_job_name(readset), samples=[readset])
        )

        jobs.append(
            concat_jobs([
                make_output_directory_job,
                picard.sort_sam(
                    unsorted_output,
                    output,
                    "coordinate"
                )
            ], name="picard_sort_sam." + readset.name + "." + readset.run + "." + readset.lane_str, samples=[readset])
        )

        return jobs

    def get_metrics_jobs(self, readset): # only if genome_folder exists
        jobs = []

        input_file_prefix = readset.bam + '.'
        input = input_file_prefix + "bam"

        output_path = os.path.join("Aligned." + readset.lane_str,'alignment',readset.sample.name,'run' + readset.run + "_" + readset.lane_str)
        output_fullpath = os.path.join(self.output_dir, output_path)

        if self.genome_folder != config.param('methylathion_ref', 'genome_dir'):
            job = picard.collect_multiple_metrics(input, input_file_prefix + "metrics",
                                                  reference_sequence=readset.reference_file)
            job.name = "picard_collect_multiple_metrics." + readset.name + ".met" + "." + readset.run + "." + readset.lane_str
            jobs.append(job)

            if readset.beds:
                coverage_bed = readset.beds[0]
                full_coverage_bed = (self.output_dir + os.sep + coverage_bed)
            else:
                coverage_bed = None
                full_coverage_bed = None

            if coverage_bed:
                if (not os.path.exists(full_coverage_bed)) and \
                        (coverage_bed not in BismarkRunProcessingAligner.downloaded_bed_files):
                    # Download the bed file
                    command = config.param('DEFAULT', 'fetch_bed_file_command').format(
                        output_directory=self.output_dir,
                        filename=coverage_bed
                    )
                    job = Job([], [full_coverage_bed], command=command, name="bed_download." + coverage_bed)
                    BismarkRunProcessingAligner.downloaded_bed_files.append(coverage_bed)
                    jobs.append(job)

                interval_list = re.sub("\.[^.]+$", ".interval_list", coverage_bed)

                if interval_list not in BismarkRunProcessingAligner.created_interval_lists:
                    # Create one job to generate the interval list from the bed file
                    ref_dict = os.path.splitext(readset.reference_file)[0] + '.dict'
                    job = tools.bed2interval_list(ref_dict, full_coverage_bed, interval_list)
                    job.name = "interval_list." + coverage_bed
                    BismarkRunProcessingAligner.created_interval_lists.append(interval_list)
                    jobs.append(job)

                job = picard.calculate_hs_metrics(input_file_prefix + "bam", input_file_prefix + "metrics.onTarget.txt",
                                                  interval_list, reference_sequence=readset.reference_file)
                job.name = "picard_calculate_hs_metrics." + readset.name + ".hs" + "." + readset.run + "." + readset.lane_str
                jobs.append(job)

            job = bvatools.depth_of_coverage(
                input,
                input_file_prefix + "metrics.targetCoverage.txt",
                full_coverage_bed,
                other_options=config.param('bvatools_depth_of_coverage', 'other_options', required=False),
                reference_genome=readset.reference_file
            )
            job.name = "bvatools_depth_of_coverage." + readset.name + ".doc" + "." + readset.run + "." + readset.lane_str
            jobs.append(job)

        name_sorted_bam = os.path.join(self.output_dir,"Aligned." + readset.lane_str,'alignment',readset.sample.name,'run' + readset.run + "_" + readset.lane_str,readset.sample.name + "." + readset.library + ".dup.sortedN.bam")
        cpg_profile = os.path.join(self.output_dir,"Aligned." + readset.lane_str,'alignment',readset.sample.name,'run' + readset.run + "_" + readset.lane_str,readset.sample.name + "." + readset.library + ".dup.sortedN.CpG_report.txt.gz")
        call_outputs = [
            os.path.join(self.output_dir,"Aligned." + readset.lane_str,'alignment',readset.sample.name,'run' + readset.run + "_" + readset.lane_str,"CpG_context_" + readset.sample.name + "." + readset.library + ".dup.sortedN.txt.gz"),
            os.path.join(self.output_dir,"Aligned." + readset.lane_str,'alignment',readset.sample.name,'run' + readset.run + "_" + readset.lane_str,readset.sample.name + "." + readset.library + ".dup.sortedN.bedGraph.gz"),
            cpg_profile
        ]
        cg_stats_output = re.sub(".CpG_report.txt.gz", ".profile.cgstats.txt", cpg_profile)
        lambda_stats_output = re.sub(".CpG_report.txt.gz", ".profile.lambda.conversion.rate.tsv", cpg_profile)
        puc19_stats_output = re.sub(".CpG_report.txt.gz", ".profile.pUC19.txt", cpg_profile)

        files_to_delete_sample_name = [os.path.join(output_fullpath, x) for x in self.get_temporary_files_sample_name_to_delete()]
        files_to_delete_sample_name = [x.replace("*", readset.sample.name + "." + readset.library) for x in files_to_delete_sample_name]
        files_to_delete_readset_name = [os.path.join(output_fullpath, x) for x in self.get_temporary_files_readset_name_to_delete()]
        files_to_delete_readset_name = [x.replace("*", readset.name + "*") for x in files_to_delete_readset_name]
        files_to_delete = files_to_delete_sample_name + files_to_delete_readset_name

        cleanup_command = "rm -rf " + " ".join(files_to_delete)
        cleanup_job = Job(command=cleanup_command)

        job = concat_jobs([
            picard.sort_sam(
                readset.bam + ".dup.bam",
                name_sorted_bam,
                "queryname"
            ),
            bismark.methyl_call_gq(
                input=name_sorted_bam,
                outputs=call_outputs,
                genome_directory=readset.aligner_reference_index
            ),
            tools.cpg_stats(
            cpg_profile,
            cg_stats_output,
            lambda_stats_output,
            puc19_stats_output
            ),
            cleanup_job
        ])
        job.name = self.get_metrics_cpg_stats_job_name(readset)
        job.is_optional_metric = False
        jobs.append(job)

        return jobs