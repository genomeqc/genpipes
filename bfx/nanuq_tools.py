#!/usr/bin/env python

################################################################################
# Copyright (C) 2014, 2015 GenAP, McGill University and Genome Quebec Innovation Centre
#
# This file is part of MUGQIC Pipelines.
#
# MUGQIC Pipelines is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MUGQIC Pipelines is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MUGQIC Pipelines.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

# MUGQIC Modules
from core.job import *


def generate_whitelist_for_dictionary(jar, dictionary_file, barcoded_bam, output_dir, outputs):
    return Job(
        [dictionary_file, barcoded_bam],
        outputs,
        [
            ['DEFAULT', "module_java"]
        ],
        command="java -classpath {jar} ca.mcgill.genome.mps.pacbio.util.GenerateWhitelistForDictionary {dictionary_file} {barcoded_bam} {output_dir}".format(
            jar=jar,
            dictionary_file=dictionary_file,
            barcoded_bam=barcoded_bam,
            output_dir=output_dir
        )
    )


def generate_read_length_histograms_for_bams(jar, ccs_file, output_dir):
    return Job(
        [ccs_file],
        [ccs_file.replace(".bam", "") + ".summary.txt"],
        [
            ['DEFAULT', "module_java"]
        ],
        command="java -classpath {jar} ca.mcgill.genome.mps.pacbio.util.GenerateReadLengthHistogramsForBams 1000 {output_dir} {ccs_file}".format(
            jar=jar,
            output_dir=output_dir,
            ccs_file=ccs_file,
        )
    )


def generate_bam_for_whitelist(jar, input, output_dir, file_type, unlabeled_file, whitelists):
    whitelist_string = " ".join(whitelists)

    inputs = [input]
    inputs.extend(whitelists)

    outputs = [unlabeled_file]

    for wl in whitelists:
        outputs.append(wl.replace(".whitelist", "." + file_type + ".bam"))

    return Job(
        inputs,
        outputs,
        [
            ['DEFAULT', "module_java"]
        ],
        command="java -classpath {jar} ca.mcgill.genome.mps.pacbio.util.GenerateBamForWhitelist {input} {output_dir} {unlabeled_file} {whitelist}".format(
            jar=jar,
            input=input,
            output_dir=output_dir,
            unlabeled_file=unlabeled_file,
            whitelist=whitelist_string
        )
    )

def compile_jobs_metrics_statistics(jar, job_input, job_output, input_directory, lane, technology):
    return Job(
        job_input,
        job_output,
        [
            ["DEFAULT", "module_java"]
        ],
        command="java -classpath {jar} ca.mcgill.genome.mps.tools.MetricsStatisticsService {input_directory} {lane} {technology}".format(
            jar=jar,
            input_directory=input_directory,
            lane=lane,
            technology=technology
        )
    )