#!/usr/bin/env python

################################################################################
# Copyright (C) 2014, 2015 GenAP, McGill University and Genome Quebec Innovation Centre
#
# This file is part of MUGQIC Pipelines.
#
# MUGQIC Pipelines is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MUGQIC Pipelines is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MUGQIC Pipelines.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

# MUGQIC Modules
from core.job import *


def dataset_create(type, output, files):
    return Job(
        files,
        [output],
        [
            ['DEFAULT', 'module_smrt_tools'],
            ['samtools_index', 'module_samtools']
        ],
        command="dataset create --generateIndices --type {type} {output} {files}".format(
            type=type,
            output=output,
            files=" ".join(files)
        )
    )


def create_inputs(input_json, output_json, genome_size, coverage):
    return Job(
        [input_json],
        [output_json],
        command="""\
sed -e "s|GENOMESIZE|{genome_size}|g" -e "s|COVERAGE|{coverage}|g" \\
  < {input_json} > {output_json}""".format(
            genome_size=genome_size,
            coverage=coverage,
            input_json=input_json,
            output_json=output_json,
        )
    )


def pbcromwell_pb_hgap4(dataset_xml, nb_cores):
    return Job(
        ["myInputs.json", dataset_xml],
        ["cromwell_out/outputs/consensus.fasta",
         "cromwell_out/outputs/p_ctg.fasta",
         "cromwell_out/outputs/coverage.gff",
         "cromwell_out/outputs/preassembly.report.json",
         "cromwell_out/outputs/coverage.report.json",
         "cromwell_out/outputs/mapping_stats.report.json",
         "cromwell_out/outputs/polished_assembly.report.json"],
        [
            ['DEFAULT', 'module_smrt_tools']
        ],
        command="pbcromwell run cromwell.workflows.pb_hgap4 --config $(readlink -f ./cromwell.conf) -e {dataset_xml} --nproc {nb_cores} -i myInputs.json".format(
            dataset_xml=dataset_xml,
            nb_cores=nb_cores,
        )
    )


def ccs(subread_filename, output_filename, number_of_threads, number_of_passes=3):
    return Job (
        [subread_filename],
        [output_filename],
        [
            ['DEFAULT', 'module_smrt_tools']
        ],
        command="ccs --min-passes {number_of_passes} --hifi-kinetics --num-threads {number_of_threads} {subread_filename} {output_filename}".format(
            number_of_passes=number_of_passes,
            number_of_threads=number_of_threads,
            subread_filename=subread_filename,
            output_filename=output_filename
        )
    )


def lima(subread_filename, output_filename, number_of_threads, fasta_filename, is_symmetric):
    mode = "--hifi-preset SYMMETRIC-ADAPTERS" if is_symmetric else "--hifi-preset ASYMMETRIC"
    return Job(
        [subread_filename],
        [output_filename],
        [
            ['DEFAULT', 'module_smrt_tools']
        ],
        command="lima --num-threads {number_of_threads} {mode} {subread_filename} {fasta_filename} {output_filename}".format(
            number_of_threads=number_of_threads,
            mode=mode,
            subread_filename=subread_filename,
            fasta_filename=fasta_filename,
            output_filename=output_filename
        )
    )


def pbcromwell_pb_basemods(subread_dataset_xml, reference_dataset_xml, nb_cores):
    return Job(
        [subread_dataset_xml, reference_dataset_xml],
        ["basemods_out/outputs/motifs.csv",
         "basemods_out/outputs/basemods.gff"],
        [
            ['DEFAULT', 'module_smrt_tools']
        ],
        command="pbcromwell run cromwell.workflows.pb_basemods --output-dir basemods_out --config $(readlink -f ./cromwell.conf) --nproc {nb_cores} -e eid_subread:{subread_dataset_xml} -e eid_ref_dataset:{reference_dataset_xml} -i basemods.json".format(
            subread_dataset_xml=subread_dataset_xml,
            nb_cores=nb_cores,
            reference_dataset_xml=reference_dataset_xml
        )
    )


def pb_microbial_analysis(dataset_xml, nb_cores):
    return Job(
        ["myInputs.json", dataset_xml],
        ["cromwell_out/outputs/final_assembly.fasta",
        "cromwell_out/outputs/assembly.rotated.polished.renamed.fsa",
         "cromwell_out/outputs/coverage.gff",
         "cromwell_out/outputs/coverage.report.json",
         "cromwell_out/outputs/mapping_stats.report.json",
         "cromwell_out/outputs/polished_assembly.report.json",
         "cromwell_out/outputs/basemods.gff",
         "cromwell_out/outputs/motifs.csv"],
        [
            ['DEFAULT', 'module_smrt_tools']
        ],
        command="pbcromwell run pb_microbial_analysis --config $(readlink -f ./cromwell.conf) -e {dataset_xml} --nproc {nb_cores} -i myInputs.json".format(
            dataset_xml=dataset_xml,
            nb_cores=nb_cores,
        )
    )


def json_metrics_to_csv(input_json_file, output_csv_file):
    return Job(
        [input_json_file],
        [output_csv_file],
        [
            ['DEFAULT', 'module_python']
        ],
        command="""\
cat {input_file} | python -c 'import json,sys;d=json.load(sys.stdin);print(",".join("\\""+a.get("name")+"\\"" for a in d.get("attributes")));print(",".join(str(a.get("value")) for a in d.get("attributes")));' > {output_file}""".format(
            input_file=input_json_file,
            output_file=output_csv_file
        )
    )