from core.config import config
from core.job import Job

def generate_fastqc_job(read1, read2, output_directory, output_list):
    threads = config.param('fastqc', 'threads', type='int', required=False)
    adapter_file = config.param('fastqc', 'adapter_sequences_file', required=False)
    return Job(
        [read1, read2],
        output_list,
        [
            ['DEFAULT', 'module_fastqc']
        ],
        command="fastqc {read1} {read2} -o {outputDirectory} -t {threads} {adapters}".format(
            threads=threads if threads > 1 else "2",
            read1=read1,
            read2=read2 if read2 else "",
            outputDirectory=output_directory,
            adapters="-a " + adapter_file if adapter_file else ""
        )
    )