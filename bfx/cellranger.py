#!/usr/bin/env python

################################################################################
# Copyright (C) 2014, 2015 GenAP, McGill University and Genome Quebec Innovation Centre
#
# This file is part of MUGQIC Pipelines.
#
# MUGQIC Pipelines is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MUGQIC Pipelines is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MUGQIC Pipelines.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

# Python Standard Modules

# MUGQIC Modules
from core.config import *
from core.job import *
from utils import utils


def count(reads1, reads2, sample_id, sample, lane_id, fastqs, transcriptome, ini_section='cellranger_count'):
    work_dir = config.param(ini_section, 'working_dir', required=True)
    work_dir += "_" + lane_id + "/"
    other_options = config.param(ini_section, 'other_options', required=False)

    return Job(
        input_files=[reads1, reads2],
        output_files=[],
        module_entries=[[ini_section, 'module_cellranger']],
        command="""\
rm -rf {work_dir}{sample_id} && \\
mkdir -p {work_dir} && \\
cd {work_dir} && \\
cellranger count \\
  --id={sample_id} \\
  --fastqs={fastqs} \\
  --sample={sample} \\
  --create-bam=true \\
  --transcriptome={transcriptome} \\
  {other_options}""".format(work_dir=work_dir, sample=sample, sample_id=sample_id, fastqs=fastqs,
                            transcriptome=transcriptome, other_options=other_options)
    )

def move_count_files(cell_work_dir, readset, ini_section='cellranger_count'):
    cell_outs_dir = os.path.join(cell_work_dir, "outs")
    default_bam_name = config.param(ini_section, 'default_bam_name', required=True)
    default_summary = config.param(ini_section, 'default_summary', required=True)
    input_bam = os.path.join(cell_outs_dir, default_bam_name) + ".bam"
    input_bai = os.path.join(cell_outs_dir, default_bam_name) + ".bam.bai"
    input_summary = os.path.join(cell_outs_dir, default_summary)
    output_bam = readset.bam + ".bam"
    output_bai = readset.bam + ".bai"
    output_summary = readset.bam + ".10x_summary.html"
    output_zip = readset.bam + "." + readset.run + "." + readset.lane_str + ".10x_outputs.zip"

    copy_job = concat_jobs([
        Job(output_files=[output_bam], command="mv " + input_bam + " " + output_bam),
        Job(output_files=[output_bai], command="mv " + input_bai + " " + output_bai),
        Job(output_files=[output_summary], command="mv " + input_summary + " " + output_summary),
        Job(output_files=[output_zip], command="cd " + cell_outs_dir + " && zip -r " + output_zip + " *")
    ])
    return copy_job
