### Falcon preassembly report

$falcon_table$

### HGAP coverage report

$hgap_coverage_table$

### HGAP mapping statistics report

$hgap_mapping_table$

### Polished assembly report

$polished_assembly_table$

