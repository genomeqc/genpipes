Introduction
------------

This document contains the description of the current Sequel Assembly analysis. The information presented here reflects the current state of the analysis as of $date$.

Assembly Method
---------------

Contigs assembly was done using the Pacbio Smrtpipe . For more information, visit the [PacBio] publications web page.

Analysis and Results
--------------------
