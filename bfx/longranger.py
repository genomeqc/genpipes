#!/usr/bin/env python

################################################################################
# Copyright (C) 2014, 2015 GenAP, McGill University and Genome Quebec Innovation Centre
#
# This file is part of MUGQIC Pipelines.
#
# MUGQIC Pipelines is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MUGQIC Pipelines is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MUGQIC Pipelines.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

# Python Standard Modules

# MUGQIC Modules
from core.config import *
from core.job import *
from utils import utils


def move_files(long_work_dir, readset, ini_section='longranger_wgs'):
    long_outs_dir = os.path.join(long_work_dir, "outs")
    default_bam_name = config.param(ini_section, 'default_bam_name', required=True)
    input_bam = os.path.join(long_outs_dir, default_bam_name) + ".bam"
    input_bai = os.path.join(long_outs_dir, default_bam_name) + ".bam.bai"
    output_bam = readset.bam + ".bam"
    output_bai = readset.bam + ".bai"
    output_zip = readset.bam + "." + readset.run + "." + readset.lane_str + ".10x_outputs.zip"

    copy_job = concat_jobs([
        Job(output_files=[output_bam], command="mv " + input_bam + " " + output_bam),
        Job(output_files=[output_bai], command="mv " + input_bai + " " + output_bai),
        Job(output_files=[output_zip], command="cd " + long_outs_dir + " && zip -r " + output_zip + " *")
    ])
    return copy_job


def wgs(reads1, reads2, sample_id, lane_id, fastqs, reference, ini_section='longranger_wgs'):
    work_dir = config.param(ini_section, 'working_dir', required=True)
    work_dir += "_" + lane_id + "/"
    other_options = config.param(ini_section, 'other_options', required=False)

    return Job(
        input_files=[reads1, reads2],
        output_files=[],
        module_entries=[[ini_section, 'module_longranger']],
        command="""\
rm -rf {work_dir}{sample_id} && \\
mkdir -p {work_dir} && \\
cd {work_dir} && \\
longranger wgs \\
  --id={sample_id} \\
  --fastqs={fastqs} \\
  --reference={reference} \\
  {other_options}""".format(work_dir=work_dir, sample_id=sample_id, fastqs=fastqs, reference=reference, other_options=other_options)
    )


def align(reads1, reads2, sample_id, lane_id, fastqs, reference, ini_section='longranger_align'):
    work_dir = config.param(ini_section, 'working_dir', required=True)
    work_dir += "_" + lane_id + "/"
    other_options = config.param(ini_section, 'other_options', required=False)

    return Job(
        input_files=[reads1, reads2],
        output_files=[],
        module_entries=[[ini_section, 'module_longranger']],
        command="""\
rm -rf {work_dir}{sample_id} && \\
mkdir -p {work_dir} && \\
cd {work_dir} && \\
longranger align \\
  --id={sample_id} \\
  --fastqs={fastqs} \\
  --reference={reference} \\
  {other_options}""".format(work_dir=work_dir, sample_id=sample_id, fastqs=fastqs, reference=reference, other_options=other_options)
    )


def targeted(reads1, reads2, sample_id, lane_id, bed_file, fastqs, reference, ini_section='longranger_targeted'):
    work_dir = config.param(ini_section, 'working_dir', required=True)
    work_dir += "_" + lane_id + "/"
    other_options = config.param(ini_section, 'other_options', required=False)

    return Job(
        input_files=[reads1, reads2, bed_file],
        output_files=[],
        module_entries=[[ini_section, 'module_longranger']],
        command="""\
rm -rf {work_dir}{sample_id} && \\
mkdir -p {work_dir} && \\
cd {work_dir} && \\
longranger targeted \\
  --id={sample_id} \\
  --fastqs={fastqs} \\
  --reference={reference} \\
  --targets={bed_file} \\
  {other_options}""".format(work_dir=work_dir, sample_id=sample_id, bed_file=bed_file, fastqs=fastqs, reference=reference,
                            other_options=other_options)
    )
